<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Codelet</title>
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="html bg">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/user-home"><i
					class="fa fa-code fa-2x" aria-hidden="true"></i> <span
					style="font-size: 1.7em;">Codelet</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <span class="hidden-xs">${firstName} ${lastName}</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
										<p>${firstName}
											${lastName}<br /> <small>Code Level: ${codeLevel}</small>
										</p></li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="${pageContext.request.contextPath}/user-home/user-settings" class="btn btn-secondary btn-flat">Settings</a>
										</div>
										<div class="pull-right">
											<a href="/CodeletApplication/"
												class="btn btn-secondary btn-flat">Sign out</a>
										</div>
									</li>
								</ul></li>
						</ul>
					</div>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
		<div id="title-header">
			<div>
				<h1 style="color: #000;">
					<i class="fa fa-files-o" aria-hidden="true"></i> Resources
				</h1>
			</div>
		</div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#secondary-nav"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-center" id="secondary-nav">
          <ul class="nav navbar-nav child">
            <li><a href="${pageContext.request.contextPath}/user-home"><i class="fa fa-rss" aria-hidden="true"></i> Forums</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/myQuestions"><i class="fa fa-question" aria-hidden="true"></i> My Questions</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/resources"><i class="fa fa-files-o" aria-hidden="true"></i> Docs &amp; Videos</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/challenges"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</a></li>
          </ul>
        </div>
			</div>
		</nav>
	</nav>
	<div class="gray-bg">
		<div class="container">
			<div class="content-bg">
				<br />
				<div class="text-center alert alert-success" style="display:${msg}">
					<h4>Your account has been updated!</h4>
				</div>
				<div class="box box-default">
					<div class="box-header with-border">
						<h6 class="box-title">
							<i class="fa fa-user" aria-hidden="true"></i> Manage Profile
						</h6>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form:form id="user" action="${pageContext.request.contextPath}/user-home/user-settings/editProfile" modelAttribute="user">
							<input type="hidden" class="form-control" name="userID"
								value="${userID}"">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label>First Name</label> <input type="text"
											class="form-control" name="firstName" value="${firstName}">
									</div>
									<div class="col-sm-6">
										<label>Last Name</label> <input type="text"
											class="form-control" name="lastName" value="${lastName}">
									</div>
								</div>
							</div>
							<br />
							<br />
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Code Level</label> <select id="avatar"
											class="form-control" name="codeLevel">
											<option value="select" disabled selected
												style="display: none;">Select</option>
											<option value="Newbie">Newbie</option>
											<option value="Intermediate">Intermediate</option>
											<option value="Guru">Guru</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Bio</label>
										<textarea class="form-control" rows="5" name="bio">${bio}</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<input name="submit" type="submit" value="Submit"
											class="form-control btn btn-secondary" />
									</div>
								</div>
							</div>
						</form:form>

					</div>
				</div>
				<br />
			</div>
		</div>
	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
</body>
</html>