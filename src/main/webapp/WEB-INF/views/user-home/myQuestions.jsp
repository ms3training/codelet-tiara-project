<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Codelet</title>
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="html bg">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/user-home"><i
					class="fa fa-code fa-2x" aria-hidden="true"></i> <span
					style="font-size: 1.7em;">Codelet</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <span class="hidden-xs">${firstName} ${lastName}</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
										<p>${firstName}
											${lastName}<br /> <small>Code Level: ${codeLevel}</small>
										</p></li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="${pageContext.request.contextPath}/user-home/user-settings" class="btn btn-secondary btn-flat">Settings</a>
										</div>
										<div class="pull-right">
											<a href="/CodeletApplication/"
												class="btn btn-secondary btn-flat">Sign out</a>
										</div>
									</li>
								</ul></li>
						</ul>
					</div>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
		<div id="title-header">
			<div>
				<h1 style="color: #000;">
					<i class="fa fa-question" aria-hidden="true"></i> My Questions
				</h1>
			</div>
		</div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#secondary-nav"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-center" id="secondary-nav">
          <ul class="nav navbar-nav child">
            <li><a href="${pageContext.request.contextPath}/user-home"><i class="fa fa-rss" aria-hidden="true"></i> Forums</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/myQuestions"><i class="fa fa-question" aria-hidden="true"></i> My Questions</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/resources"><i class="fa fa-files-o" aria-hidden="true"></i> Docs &amp; Videos</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/challenges"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</a></li>
          </ul>
        </div>
			</div>
		</nav>
	</nav>
	<div class="gray-bg">
		<div class="container">
			<div class="content-bg">
				<br />
				<form:form id="searchForumForm" action="${pageContext.request.contextPath}/user-home/myQuestions/search"
					method="post" modelAttribute="forum">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-8">
								<input type="text" class="form-control"
									placeholder="e.g. Spring Framework" name="title">
							</div>
							<div class="col-sm-4">
								<input name="submit" type="submit" value="Search"
									class="form-control btn btn-secondary" />
								<!-- <button class="form-control btn btn-secondary" type="submit">Search <i class="fa fa-search" aria-hidden="true"></i></button> -->
							</div>
						</div>
					</div>
				</form:form>
				<br />
				<div>

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#myQuestions"
							aria-controls="myQuestions" role="tab" data-toggle="tab">My
								Questions</a></li>
						<li role="presentation"><a href="#myFavorites"
							aria-controls="myFavorites" role="tab" data-toggle="tab">My
								Favorites</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active"
							id="myQuestions">
							<br />
							<div id="searchResults">
								<h3 class="text-center">${errorMessage}</h3>
								<c:forEach items="${questionList}" var="question">
									<div class="card">
										<h4>
											<spring:url
												value="/user-home/question/${question.questionID}"
												var="questionURL"></spring:url>
											<a href="${questionURL}" class="pull-left">${question.title}</a>
										</h4>
										<span class="label label-questions pull-right"><i
											class="fa fa-thumbs-up" aria-hidden="true"></i>
											${question.likes}</span> <br />
										<h6 class="mainorange">Posted: ${question.postDate}</h6>
										<p>${question.content}</p>
										<br />
										<button type="button" class="btn btn-info btn-sm"
											data-toggle="modal" data-target="#editModal${question.questionID}">
											<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
										</button>
										<button type="button" class="btn btn-danger btn-sm"
											data-toggle="modal" data-target="#deleteModal${question.questionID}">
											<i class="fa fa-trash" aria-hidden="true"></i> DELETE
										</button>
									</div>
									<br />
									<!-- Button trigger modal -->

									<!-- DELETE -->
									<div class="modal fade" id="deleteModal${question.questionID}" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header bgmainblue">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title text-center">Delete Question?</h4>
												</div>
												<div class="modal-body text-center">
													<p>Are you sure you want to delete this question?</p>
													<form:form id="questionForm" action="" method="post"
														modelAttribute="question">
														<input type="hidden" name="questionID" id="questionID"
															class="form-control" value="${question.questionID}"
															required="required" placeholder="" />
														<div class="form-group">
															<div class="row">
																<div class="col-sm-4">
																	<input name="submit" type="submit" value="Submit"
																		class="form-control btn btn-secondary" />
																</div>
																<div class="col-sm-4">
																	<button type="button"
																		class="form-control btn btn-default"
																		data-dismiss="modal">Cancel</button>
																</div>
															</div>
														</div>
													</form:form>
												</div>
											</div>
										</div>
									</div>
									<!-- EDIT -->
									<div class="modal fade" id="editModal${question.questionID}" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header bgmainblue">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title text-center">Edit Question</h4>
												</div>
												<div class="modal-body">
													<br />
													<form:form id="questionForm" action="" method="post"
														modelAttribute="question">

														<input type="hidden" name="questionID" id="questionID"
															class="form-control" value="${question.questionID}"
															required="required" placeholder="" />

														<div class="form-group">
															<div class="row">
																<div class="col-sm-12">
																	<label>Title</label> <input type="text" name="title"
																		id="title" class="form-control" required="required"
																		placeholder="" value="${question.title}" />

																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-12">
																	<label>Question Content</label>
																	<textarea class="form-control" rows="5" name="content">${question.content}</textarea>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-4">
																	<input name="submit" type="submit" value="Submit"
																		class="form-control btn btn-secondary" />
																</div>
																<div class="col-sm-4">
																	<button type="button"
																		class="form-control btn btn-default"
																		data-dismiss="modal">Cancel</button>
																</div>
															</div>
														</div>
													</form:form>
													<br />
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="myFavorites">
							<br />
								<div id="searchResults">
									<h3 class="text-center">${errorMessage}</h3>
									<c:forEach items="${favoriteList}" var="question">
										<div class="card">
											<h4>
												<spring:url value="/user-home/question/${question.questionID}" var="questionURL"></spring:url><a href="${questionURL}" class="pull-left">${question.title}</a>
											</h4>
											<span class="label label-questions pull-right"><i
												class="fa fa-thumbs-up" aria-hidden="true"></i> ${question.likes}</span>
											<br />
														<h6 class="mainorange">Posted: ${question.postDate}</h6>
											<p>${question.content}</p>
										</div>
										<br />
									</c:forEach>
								</div>
				
				
							<%-- <div id="searchResults">
								<h3 class="text-center">${errorMessage}</h3>
								<c:forEach items="${favoriteList}" var="favorite">
									<div class="card">
										<h4>
											<spring:url
												value="/user-home/question/${favorite.questionID}"
												var="questionURL"></spring:url>
											<a href="${questionURL}" class="pull-left">${favorite.title}</a>
										</h4>
										<h5>${favorite.postDate}</h5>
										<span class="label label-questions pull-right"><i
											class="fa fa-comments" aria-hidden="true"></i>${favorite.likes}</span>
										<br /> <br />
										<p>${favorite.content}</p>
									</div>
									<br />
								</c:forEach>
							</div> --%>
						</div>
					</div>
				</div>


				<div>
					<br />
					<div id="pagination" class="text-center">
						<nav>
							<ul class="pagination">
								<li class="page-item disabled"><a class="page-link"
									href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
								</a></li>
								<li class="page-item active"><a class="page-link" href="#">1
										<span class="sr-only">(current)</span>
								</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><a class="page-link" href="#">5</a></li>
								<li class="page-item"><a class="page-link" href="#"
									aria-label="Next"> <span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
								</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
</body>
</html>




