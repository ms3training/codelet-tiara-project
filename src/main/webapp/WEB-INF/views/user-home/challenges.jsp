<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Codelet</title>
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="html bg">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/user-home"><i
					class="fa fa-code fa-2x" aria-hidden="true"></i> <span
					style="font-size: 1.7em;">Codelet</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <span class="hidden-xs">${firstName} ${lastName}</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
										<p>${firstName}
											${lastName}<br /> <small>Code Level: ${codeLevel}</small>
										</p></li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="${pageContext.request.contextPath}/user-home/user-settings" class="btn btn-secondary btn-flat">Settings</a>
										</div>
										<div class="pull-right">
											<a href="/CodeletApplication/"
												class="btn btn-secondary btn-flat">Sign out</a>
										</div>
									</li>
								</ul></li>
						</ul>
					</div>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
		<div id="title-header">
			<div>
				<h1 style="color: #000;">
					<i class="fa fa-trophy" aria-hidden="true"></i> Challenges
				</h1>
			</div>
		</div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#secondary-nav"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-center" id="secondary-nav">
          <ul class="nav navbar-nav child">
            <li><a href="${pageContext.request.contextPath}/user-home"><i class="fa fa-rss" aria-hidden="true"></i> Forums</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/myQuestions"><i class="fa fa-question" aria-hidden="true"></i> My Questions</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/resources"><i class="fa fa-files-o" aria-hidden="true"></i> Docs &amp; Videos</a></li>
            <li><a href="${pageContext.request.contextPath}/user-home/challenges"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</a></li>
          </ul>
        </div>
			</div>
		</nav>
	</nav>
	<div class="gray-bg">
		<div class="container">
			<div class="content-bg">
				<br />
				<form:form id="searchChallengeForm" action="${pageContext.request.contextPath}/user-home/challenges/searchChallenges"
					method="post" modelAttribute="forum">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-8">
								<input type="text" class="form-control"
									placeholder="e.g. Spring Framework" name="title">
							</div>
							<div class="col-sm-3">
            <input name="submit" type="submit" value="Search"
									class="form-control btn btn-secondary" />
            <!-- <button class="form-control btn btn-secondary" type="submit">Search <i class="fa fa-search" aria-hidden="true"></i></button> -->
            </div>
            <div class="col-sm-1">
            <form:form id="searchForumForm" action="${pageContext.request.contextPath}/user-home/clearChallengeFilters" method="post" modelAttribute="forum"><button type="submit" class="form-control btn btn-danger" data-toggle="tooltip" data-placement="right" title="Clear Filters"><i class="fa fa-times" aria-hidden="true"></i></button></form:form>
            
            </div>
          </div>
        </div>
        </form:form>
				<br />
				<div id="categories">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-2">
								<form:form id="gameDev" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control"
										value="Game Development" name="category">
									<button class="forum-category green-category" type="submit">
										<i class="fa fa-gamepad fa-3x" aria-hidden="true"></i><br />Game
										Development<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="database" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control" value="Database"
										name="category">
									<button class="forum-category lblue-category" type="submit">
										<i class="fa fa-database fa-3x" aria-hidden="true"></i><br />Database<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="mobileDev" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control"
										value="Mobile Development" name="category">
									<button class="forum-category pink-category" type="submit">
										<i class="fa fa-mobile fa-3x" aria-hidden="true"></i><br />Mobile
										Development<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="languages" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control" value="Languages"
										name="category">
									<button class="forum-category yellow-category" type="submit">
										<i class="fa fa-file-code-o fa-3x" aria-hidden="true"></i><br />Languages<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="testing" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control"
										value="Software Testing" name="category">
									<button class="forum-category teal-category" type="submit">
										<i class="fa fa-list-alt fa-3x" aria-hidden="true"></i><br />Software
										Testing<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="devTools" action="${pageContext.request.contextPath}/user-home/challenges/filterChallengeCategory"
									method="post" modelAttribute="forum">
									<input type="hidden" class="form-control"
										value="Development Tools" name="category">
									<button class="forum-category purple-category" type="submit">
										<i class="fa fa-wrench fa-3x" aria-hidden="true"></i><br />Development
										Tools<br />
									</button>
								</form:form>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="box box-default collapsed-box">
					<div class="box-header with-border">
						<h6 class="box-title">
							<i class="fa fa-trophy" aria-hidden="true"></i> Add New Challenge
						</h6>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-plus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<br />
						<form:form id="challengeForm" action="" method="post"
							modelAttribute="challenge">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Title</label> <input type="text" name="title"
											id="title" class="form-control" required="required"
											placeholder="" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-3 col-sm-offset-3">
										<label>Category</label> <select class="form-control"
											name="category">
											<option value="Game Development">Game Development</option>
											<option value="Database">Database</option>
											<option value="Mobile Development">Mobile
												Development</option>
											<option value="Languages">Languages</option>
											<option value="QA Testing">QA Testing</option>
											<option value="Development Tools">Development Tools</option>
										</select>
									</div>
									<div class="col-sm-3">
										<label>Type</label> <select class="form-control" name="type">
											<option value="Coding Challenge">Coding Challenge</option>
											<option value="Interview question">Interview
												question</option>

										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Question</label>
										<textarea class="form-control" rows="5" name="question">
										</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Answer</label>
										<textarea class="form-control" rows="5" name="answer">
										</textarea>
									</div>
								</div>
							</div>
							<div class="col-sm-2 col-sm-offset-3">
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
							</div>
						</form:form>
						<br /> <br /> <br /> <br />
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<br />
				<div id="searchResults">
					<h3 class="text-center">${errorMessage}</h3>
					<c:forEach items="${challengeList}" var="challenge">
						<div class="card">
							<h4 class="pull-left">${challenge.title}</h4>
							<span class="label label-questions pull-right">${challenge.category}</span>
							<br /> <br />
							<p>${challenge.question}</p>
							<br /> 
							<button type="button" class="btn btn-info btn-sm"
									data-toggle="modal" data-target="#answerModal${challenge.challengeID}">
									<i class="fa fa-info" aria-hidden="true"></i> ANSWER
								</button>
						</div>
						<br />
							<div class="modal fade" id="answerModal${challenge.challengeID}" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header bgmainblue">
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title text-center">Answer</h4>
								</div>
								<div class="modal-body text-center">
									<p>${challenge.answer}</p>
								</div>
							</div>
						</div>
					</div>
					</c:forEach>
				</div>
				<br />
				<div id="pagination" class="text-center">
					<nav>
						<ul class="pagination">
							<li class="page-item disabled"><a class="page-link" href="#"
								aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previous</span>
							</a></li>
							<li class="page-item active"><a class="page-link" href="#">1
									<span class="sr-only">(current)</span>
							</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#"
								aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span
									class="sr-only">Next</span>
							</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
</body>
</html>




