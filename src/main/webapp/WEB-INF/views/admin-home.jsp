<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Codelet</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
     <spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
        <header class="main-header">
            <a href="${pageContext.request.contextPath}/admin-home/" class="logo">
                <span class="logo-mini"><b><i class="fa fa-code" aria-hidden="true"></i></b></span>
                <span class="logo-lg"><b>Codelet</b></span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li><a id="lblAdminName">${firstName} ${lastName}</a></li>
                        <li>
                            <a id="lblAdminLogout" href="/CodeletApplication/"><i class="fa fa-sign-out"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu">
                    <li class="header text-center">ADMIN CONTROLS</li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/">
                            <i class="fa fa-dashboard"></i><span>Dashboard</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/forums">
                            <i class="fa fa-rss"></i><span>Forums</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/questions">
                            <i class="fa fa-question"></i><span>Questions</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/resources">
                            <i class="fa fa-files-o"></i><span>Resources</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/challenges">
                            <i class="fa fa-trophy"></i><span>Challenges</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/administrators">
                            <i class="fa fa-user"></i><span>Manage Administrators</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="${pageContext.request.contextPath}/admin-home/settings">
                            <i class="fa fa-cogs"></i><span>Settings</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
             <h1 class="text-center">Welcome ${firstName} ${lastName}!</h1>
            <br />
    		<br />
    		<br />
    		<br />
    <div class="row">
        <div class="col-sm-12">
            <div class="box mainblue-top">
                <div class="box-header with-border">
                    <h3 class="box-title">Quick Links</h3>
                </div>
                <div class="box-body center">
                    <div class="row">
                        <div id="adminForum" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/forums" class="info-box bgblue"  data-toggle="tooltip" data-placement="bottom" title="Forums">
                                <span class="info-box-icon"><i class="fa fa-rss"></i></span><br/><h2 class="text-center">FORUMS</h2>
                            </a>
                        </div>
                        <div id="adminQuestions" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/questions" class="info-box bgpink" data-toggle="tooltip" data-placement="bottom" title="Questions">
                                <span class="info-box-icon"><i class="fa fa-question"></i></span><br/><h2 class="text-center">QUESTIONS</h2>
                            </a>
                        </div>
                         </div>
                        <div class="row">
                        <div id="adminResources" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/resources" class="info-box bgteal" data-toggle="tooltip" data-placement="bottom" title="Resources">
                                <span class="info-box-icon"><i class="fa fa-files-o"></i></span><br/><h2 class="text-center">RESOURCES</h2>
                            </a>
                        </div>
                        <div id="adminChallenges" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/challenges" class="info-box bglight-blue" data-toggle="tooltip" data-placement="bottom" title="Challenges">
                                <span class="info-box-icon"><i class="fa fa-trophy"></i></span><br/><h2 class="text-center">CHALLENGES</h2>
                            </a>
                        </div>
                         </div>
                        <div class="row">
                        <div id="adminAdmin" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/administrators" class="info-box bgpurple" data-toggle="tooltip" data-placement="bottom" title="Administrators">
                                <span class="info-box-icon"><i class="fa fa-user"></i></span><br/><h2 class="text-center">ADMINISTRATORS</h2>
                            </a>
                        </div>
                        <div id="adminSettings" class="col-sm-6">
                            <a href="${pageContext.request.contextPath}/admin-home/settings" class="info-box bggreen" data-toggle="tooltip" data-placement="bottom" title="Settings">
                                <span class="info-box-icon"><i class="fa fa-cogs"></i></span><br/><h2 class="text-center">SETTINGS</h2>
                            </a>
                        </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    <spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
	<script>
	  $(function () {
	    //Enable iCheck plugin for checkboxes
	    //iCheck for checkbox and radio inputs
	    $('.mailbox-messages input[type="checkbox"]').iCheck({
	      checkboxClass: 'icheckbox_flat-blue',
	      radioClass: 'iradio_flat-blue'
	    });

	    //Enable check and uncheck all functionality
	    $(".checkbox-toggle").click(function () {
	      var clicks = $(this).data('clicks');
	      if (clicks) {
	        //Uncheck all checkboxes
	        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
	        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
	      } else {
	        //Check all checkboxes
	        $(".mailbox-messages input[type='checkbox']").iCheck("check");
	        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
	      }
	      $(this).data("clicks", !clicks);
	    });

	    //Handle starring for glyphicon and font awesome
	    $(".mailbox-star").click(function (e) {
	      e.preventDefault();
	      //detect type
	      var $this = $(this).find("a > i");
	      var glyph = $this.hasClass("glyphicon");
	      var fa = $this.hasClass("fa");

	      //Switch states
	      if (glyph) {
	        $this.toggleClass("glyphicon-star");
	        $this.toggleClass("glyphicon-star-empty");
	      }

	      if (fa) {
	        $this.toggleClass("fa-star");
	        $this.toggleClass("fa-star-o");
	      }
	    });
	  });
	</script>
</body>
</html>