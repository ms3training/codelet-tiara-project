<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Codelet</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport" />
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<header class="main-header">
		<a href="${pageContext.request.contextPath}/admin-home/" class="logo"> <span
			class="logo-mini"><b><i class="fa fa-code"
					aria-hidden="true"></i></b></span> <span class="logo-lg"><b>Codelet</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a id="lblAdminName">${firstName} ${lastName}</a></li>
					<li><a id="lblAdminLogout" href="/CodeletApplication/"><i class="fa fa-sign-out"></i></a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header text-center">ADMIN CONTROLS</li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/"> <i
						class="fa fa-dashboard"></i><span>Dashboard</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/forums"> <i
						class="fa fa-rss"></i><span>Forums</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/questions"> <i
						class="fa fa-question"></i><span>Questions</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/resources"> <i
						class="fa fa-files-o"></i><span>Resources</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/challenges"> <i
						class="fa fa-trophy"></i><span>Challenges</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/administrators"> <i
						class="fa fa-user"></i><span>Manage Administrators</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/settings"> <i
						class="fa fa-cogs"></i><span>Settings</span>
				</a></li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		<h1 class="text-center">
			<i class="fa fa-user"></i> Manage Administrators
		</h1>
		<br /> <br />
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Add Administrators</h2>
					</div>
					<div class="box-body center">
						<form:form id="adminForm">
							<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"
													aria-hidden="true"></i></span>
												<input type="text" name="firstName"
													id="firstName" class="form-control" required="required"
													placeholder="first name" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"
													aria-hidden="true"></i></span>
												<input name="lastName" id="lastName"
													class="form-control" required="required"
													placeholder="last name" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i
													class="fa fa-envelope" aria-hidden="true"></i></span> <input
													type="text" class="form-control" placeholder="email"
													name="email" required>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-lock"
													aria-hidden="true"></i></span> <input type="password"
													class="form-control" placeholder="password" name="password"
													required>
											</div>
										</div>
									</div>
								</div>
								<div class="center">
								<img id="newbie" src="<c:url value="/resources/baby.png"/>"  alt="Newbie" class="img-circle">
								<img id="intermediate" src="<c:url value="/resources/middle.png"/>"  alt="Intermediate" class="img-circle">
								<img id="guru" src="<c:url value="/resources/guru.ico"/>" alt="Guru" class="img-circle">
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Code Level</label> 
										<select id="avatar" class="form-control" name="codeLevel">
										  <option value="" disabled selected style="display:none;">Select</option>
										  <option value="Newbie">Newbie</option>
											<option value="Intermediate">Intermediate</option>
											<option value="Guru">Guru</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Bio</label>
										<textarea class="form-control" rows="5" name="bio">
										</textarea>
									</div>
								</div>
							</div>
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
						</form:form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Manage Administrators</h2>
					</div>
					<div class="box-body center">
						<div>
			<table class="table table-responsive table-hover table-bordered">
					<tr>
						<th><h5><b>First Name</b></h5></th>
						<th><h5><b>Last Name</b></h5></th>
						<th><h5><b>Email</b></h5></th>
						<th><h5><b>Status</b></h5></th>
						<th><h5><b>Delete</b></h5></th>
					</tr>
				<c:forEach items="${userList}" var="user">
					<tbody>
						<tr>
							<td>${user.firstName}</td>
							<td>${user.lastName}</td>
							<td>${user.email}</td>
							<td>${user.accountStatus}</td>
							<td><button type="button" class="btn btn-danger btn-sm"
									data-toggle="modal" data-target="#deleteModal${user.userID}">
									<i class="fa fa-trash" aria-hidden="true"></i> DELETE
								</button></td>
						</tr>
					</tbody>
							<!-- DELETE -->
					<div class="modal fade" id="deleteModal${user.userID}" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header bgmainblue">
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title text-center">Delete Administrator?</h4>
								</div>
								<div class="modal-body text-center">
									<p>Are you sure you want to delete this Administrator?</p>
									<form:form id="adminForm" action="" method="post"
										modelAttribute="user">
										<input type="hidden" name="userID" id="userID"
											class="form-control" value="${user.userID}"
											required="required" placeholder="" />
										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<input name="submit" type="submit" value="Submit"
														class="form-control btn btn-secondary" />
												</div>
												<div class="col-sm-4">
													<button type="button" class="form-control btn btn-default"
														data-dismiss="modal">Cancel</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</table>
		</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
<script>
	$(document).ready(function(){
	    $('#avatar').on('change', function() {
	      if ( this.value == 'Newbie')
	      {
	        $("#newbie").show();
	        $("#intermediate").hide();
	        $("#guru").hide();
	      }
	      else if ( this.value == 'Intermediate')
	      {
	        $("#newbie").hide();
	        $("#intermediate").show();
	        $("#guru").hide();
	      }
	      else
	      {
	    	$("#newbie").hide();
	    	$("#intermediate").hide();
	        $("#guru").show();
	      }
	    });
	});
	</script>
</body>
</html>