<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Codelet</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport" />
<spring:url value="/resources/css/Main.css" var="mainCss" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<header class="main-header">
		<a href="${pageContext.request.contextPath}/admin-home/" class="logo"> <span
			class="logo-mini"><b><i class="fa fa-code"
					aria-hidden="true"></i></b></span> <span class="logo-lg"><b>Codelet</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a id="lblAdminName">${firstName} ${lastName}</a></li>
					<li><a id="lblAdminLogout" href="/CodeletApplication/"><i
							class="fa fa-sign-out"></i></a></li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header text-center">ADMIN CONTROLS</li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/"> <i
						class="fa fa-dashboard"></i><span>Dashboard</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/forums"> <i
						class="fa fa-rss"></i><span>Forums</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/questions"> <i
						class="fa fa-question"></i><span>Questions</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/resources"> <i
						class="fa fa-files-o"></i><span>Resources</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/challenges"> <i
						class="fa fa-trophy"></i><span>Challenges</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/administrators"> <i
						class="fa fa-user"></i><span>Manage Administrators</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/settings"> <i
						class="fa fa-cogs"></i><span>Settings</span>
				</a></li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		<h1 class="text-center">
			<i class="fa fa-cog"></i> Settings
		</h1>
		<br /> <br />
		<div class="text-center alert alert-success" style="display:${msg}"><h4>Your account has been updated!</h4></div>
			<div class="box box-default">
					<div class="box-header with-border">
						<h6 class="box-title">
							<i class="fa fa-user" aria-hidden="true"></i> Manage Profile
						</h6>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
								<form:form id="user" action="" modelAttribute="user">
								<input type="hidden" class="form-control" name="userID" value="${userID}"">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
							<label>First Name</label>
								<input type="text" class="form-control" name="firstName" value="${firstName}">
							</div>
							<div class="col-sm-6">
							<label>Last Name</label>
								<input type="text" class="form-control" name="lastName" value="${lastName}">
							</div>
						</div>
					</div>
				
					<br/>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label>Code Level</label> <select id="avatar"
									class="form-control" name="codeLevel">
									<option value="select" disabled selected style="display: none;">Select</option>
									<option value="Newbie">Newbie</option>
									<option value="Intermediate">Intermediate</option>
									<option value="Guru">Guru</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label>Bio</label>
								<textarea class="form-control" rows="5" name="bio">${bio}</textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-4">
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
							</div>
						</div>
					</div>
				</form:form>
					
					</div>
					</div>
			<br/>

	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
</body>
</html>