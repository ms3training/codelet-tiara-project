<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Codelet</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport" />
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<header class="main-header">
		<a href="${pageContext.request.contextPath}/admin-home/" class="logo"> <span
			class="logo-mini"><b><i class="fa fa-code"
					aria-hidden="true"></i></b></span> <span class="logo-lg"><b>Codelet</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a id="lblAdminName">${firstName} ${lastName}</a></li>
					<li><a id="lblAdminLogout" href="/CodeletApplication/"><i
							class="fa fa-sign-out"></i></a></li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header text-center">ADMIN CONTROLS</li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/"> <i
						class="fa fa-dashboard"></i><span>Dashboard</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/forums"> <i
						class="fa fa-rss"></i><span>Forums</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/questions"> <i
						class="fa fa-question"></i><span>Questions</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/resources"> <i
						class="fa fa-files-o"></i><span>Resources</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/challenges"> <i
						class="fa fa-trophy"></i><span>Challenges</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/administrators"> <i
						class="fa fa-user"></i><span>Manage Administrators</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/settings"> <i
						class="fa fa-cogs"></i><span>Settings</span>
				</a></li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		<h1 class="text-center">
			<i class="fa fa-questions"></i> Questions
		</h1>
		<br /> <br />
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Filter Questions</h2>
					</div>
					<div class="box-body center">
						<form:form id="searchForumForm"
							action="${pageContext.request.contextPath}/admin-home/questions/searchQuestions"
							method="post" modelAttribute="forum">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-8">
										<input type="text" class="form-control"
											placeholder="e.g. Spring Framework" name="title">
									</div>
									<div class="col-sm-4">
										<!-- 										<input name="submit" type="submit" value="Search"
											class="form-control btn btn-secondary" /> -->
										<button class="form-control btn btn-secondary" type="submit">
											Search <i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</div>
								</div>
							</div>
						</form:form>
							<br />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Manage Questions</h2>
					</div>
					<div class="box-body center">
						<div>
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#all"
									aria-controls="all" role="tab" data-toggle="tab">All
										questions</a></li>
								<li role="presentation"><a href="#flagged"
									aria-controls="flagged" role="tab" data-toggle="tab">Flagged
										Questions</a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="all">
									<table id="allQuestions"
										class="table table-responsive table-hover table-bordered">
										<tr>
											<th><h5>
													<b>Title</b>
												</h5></th>
											<th><h5>
													<b>Content</b>
												</h5></th>
											<th><h5>
													<b>Likes</b>
												</h5></th>
											<th><h5>
													<b>Date Posted</b>
												</h5></th>
											<th><h5>
													<b>Status</b>
												</h5></th>
											<th><h5>
													<b>Edit</b>
												</h5></th>
											<th><h5>
													<b>Delete</b>
												</h5></th>
										</tr>
										<c:forEach items="${questionList}" var="question">
											<tbody>
												<c:choose>
													<c:when test="${question.flagged == 'Yes'}">
														<c:set var="statusDescritpion"
															value="This question was flagged! Review the content and determine whether to edit or delete it." />
														<tr class="danger">
															<td>${question.title}</td>
															<td>${question.content}</td>
															<td>${question.likes}</td>
															<td>${question.postDate}</td>
															<td><button type="button"
																	class="btn btn-cherry btn-sm" data-toggle="modal"
																	data-target="#statusModal${question.questionID}">
																	<i class="fa fa-flag" aria-hidden="true"></i> FLAGGED
																</button></td>
															<td><button type="button"
																	class="btn btn-info btn-sm" data-toggle="modal"
																	data-target="#editModal${question.questionID}">
																	<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
																</button></td>
															<td><button type="button"
																	class="btn btn-danger btn-sm" data-toggle="modal"
																	data-target="#deleteModal${question.questionID}">
																	<i class="fa fa-trash" aria-hidden="true"></i> DELETE
																</button></td>
														</tr>
														<!-- STATUS -->
														<div class="modal fade"
															id="statusModal${question.questionID}" tabindex="-1"
															role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<div class="modal-header bgmainblue">
																		<button type="button" class="close"
																			data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title text-center">Status</h4>
																	</div>
																	<div class="modal-body ">
																		<div class="alert alert-danger">
																			<p class="text-center">
																				<c:out value="${statusDescritpion}" />
																			</p>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-secondary"
																			data-dismiss="modal">Close</button>
																	</div>
																</div>
															</div>
														</div>
													</c:when>
													<c:otherwise>
														<c:set var="statusDescritpion"
															value="This question is good with no flags :)" />
														<tr>
															<td>${question.title}</td>
															<td>${question.content}</td>
															<td>${question.likes}</td>
															<td>${question.postDate}</td>
															<td><button type="button"
																	class="btn btn-success btn-sm" data-toggle="modal"
																	data-target="#statusModal${question.questionID}">
																	<i class="fa fa-thumbs-up" aria-hidden="true"></i>
																	U FINE
																</button></td>
															<td><button type="button"
																	class="btn btn-info btn-sm" data-toggle="modal"
																	data-target="#editModal${question.questionID}">
																	<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
																</button></td>
															<td><button type="button"
																	class="btn btn-danger btn-sm" data-toggle="modal"
																	data-target="#deleteModal${question.questionID}">
																	<i class="fa fa-trash" aria-hidden="true"></i> DELETE
																</button></td>
														</tr>
														<!-- STATUS -->
														<div class="modal fade"
															id="statusModal${question.questionID}" tabindex="-1"
															role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<div class="modal-header bgmainblue">
																		<button type="button" class="close"
																			data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title text-center">Status</h4>
																	</div>
																	<div class="modal-body ">
																		<div class="alert alert-success">
																			<p class="text-center">
																				<c:out value="${statusDescritpion}" />
																			</p>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-secondary"
																			data-dismiss="modal">Close</button>
																	</div>
																</div>
															</div>
														</div>
													</c:otherwise>
												</c:choose>
											</tbody>
											<!-- DELETE -->
											<div class="modal fade"
												id="deleteModal${question.questionID}" tabindex="-1"
												role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header bgmainblue">
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
															<h4 class="modal-title text-center">Delete Question?</h4>
														</div>
														<div class="modal-body text-center">
															<p>Are you sure you want to delete this question?</p>
															<form:form id="questionForm" action="" method="post"
																modelAttribute="question">
																<input type="hidden" name="questionID" id="questionID"
																	class="form-control" value="${question.questionID}"
																	required="required" placeholder="" />
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-4">
																			<input name="submit" type="submit" value="Submit"
																				class="form-control btn btn-secondary" />
																		</div>
																		<div class="col-sm-4">
																			<button type="button"
																				class="form-control btn btn-default"
																				data-dismiss="modal">Cancel</button>
																		</div>
																	</div>
																</div>
															</form:form>
														</div>
													</div>
												</div>
											</div>

											<!-- EDIT -->
											<div class="modal fade" id="editModal${question.questionID}"
												tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header bgmainblue">
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
															<h4 class="modal-title text-center">Edit Question</h4>
														</div>
														<div class="modal-body">
															<br />
															<form:form id="questionForm" action="" method="post"
																modelAttribute="question">

																<input type="hidden" name="questionID" id="questionID"
																	class="form-control" value="${question.questionID}"
																	required="required" placeholder="" />

																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-12">
																			<label>Title</label> <input type="text" name="title"
																				id="title" class="form-control" required="required"
																				placeholder="" value="${question.title}" />

																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-12">
																			<label>Question Content</label>
																			<textarea class="form-control" rows="5"
																				name="content">${question.content}</textarea>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-4">
																			<input name="submit" type="submit" value="Submit"
																				class="form-control btn btn-secondary" />
																		</div>
																		<div class="col-sm-4">
																			<button type="button"
																				class="form-control btn btn-default"
																				data-dismiss="modal">Cancel</button>
																		</div>
																	</div>
																</div>
															</form:form>
															<br />
														</div>
													</div>
												</div>
											</div>
										</c:forEach>
									</table>
								</div>
								<div role="tabpanel" class="tab-pane" id="flagged">
									<table id="flaggedQuestions"
										class="table table-responsive table-hover table-bordered">
										<tr>
											<th><h5>
													<b>Title</b>
												</h5></th>
											<th><h5>
													<b>Content</b>
												</h5></th>
											<th><h5>
													<b>Likes</b>
												</h5></th>
											<th><h5>
													<b>Date Posted</b>
												</h5></th>
											<th><h5>
													<b>Status</b>
												</h5></th>
											<th><h5>
													<b>Edit</b>
												</h5></th>
											<th><h5>
													<b>Delete</b>
												</h5></th>
										</tr>
										<c:forEach items="${flaggedList}" var="flagged">
											<tbody>
												<c:set var="statusDescritpion"
													value="This question was flagged! Review the content and determine whether to edit or delete it." />
												<tr class="danger">
													<td>${flagged.title}</td>
													<td>${flagged.content}</td>
													<td>${flagged.likes}</td>
													<td>${flagged.postDate}</td>
													<td><button type="button"
															class="btn btn-cherry btn-sm" data-toggle="modal"
															data-target="#statusFlagModal${flagged.questionID}">
															<i class="fa fa-flag" aria-hidden="true"></i> FLAGGED
														</button></td>
													<td><button type="button" class="btn btn-info btn-sm"
															data-toggle="modal"
															data-target="#editFlagModal${flagged.questionID}">
															<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
														</button></td>
													<td><button type="button"
															class="btn btn-danger btn-sm" data-toggle="modal"
															data-target="#deleteFlagModal${flagged.questionID}">
															<i class="fa fa-trash" aria-hidden="true"></i> DELETE
														</button></td>
												</tr>


											</tbody>
											<!-- DELETE -->
											<div class="modal fade"
												id="deleteFlagModal${flagged.questionID}" tabindex="-1"
												role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header bgmainblue">
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
															<h4 class="modal-title text-center">Delete Question?</h4>
														</div>
														<div class="modal-body text-center">
															<p>Are you sure you want to delete this question?</p>
															<form:form id="questionForm" action="" method="post"
																modelAttribute="question">
																<input type="hidden" name="questionID" id="questionID"
																	class="form-control" value="${flagged.questionID}"
																	required="required" placeholder="" />
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-4">
																			<input name="submit" type="submit" value="Submit"
																				class="form-control btn btn-secondary" />
																		</div>
																		<div class="col-sm-4">
																			<button type="button"
																				class="form-control btn btn-default"
																				data-dismiss="modal">Cancel</button>
																		</div>
																	</div>
																</div>
															</form:form>
														</div>
													</div>
												</div>
											</div>

											<!-- STATUS -->
											<div class="modal fade"
												id="statusFlagModal${flagged.questionID}" tabindex="-1"
												role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header bgmainblue">
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
															<h4 class="modal-title text-center">Status</h4>
														</div>
														<div class="modal-body ">
															<div class="alert alert-danger">
																<p class="text-center">
																	<c:out value="${statusDescritpion}" />
																</p>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary"
																data-dismiss="modal">Close</button>
														</div>
													</div>
												</div>
											</div>

											<!-- EDIT -->
											<div class="modal fade"
												id="editFlagModal${flagged.questionID}" tabindex="-1"
												role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header bgmainblue">
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
															<h4 class="modal-title text-center">Edit Question</h4>
														</div>
														<div class="modal-body">
															<br />
															<form:form id="questionForm" action="" method="post"
																modelAttribute="question">

																<input type="hidden" name="questionID" id="questionID"
																	class="form-control" value="${flagged.questionID}"
																	required="required" placeholder="" />

																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-12">
																			<label>Title</label> <input type="text" name="title"
																				id="title" class="form-control" required="required"
																				placeholder="" value="${flagged.title}" />

																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-12">
																			<label>Question Content</label>
																			<textarea class="form-control" rows="5"
																				name="content">${question.content}</textarea>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="row">
																		<div class="col-sm-4">
																			<input name="submit" type="submit" value="Submit"
																				class="form-control btn btn-secondary" />
																		</div>
																		<div class="col-sm-4">
																			<button type="button"
																				class="form-control btn btn-default"
																				data-dismiss="modal">Cancel</button>
																		</div>
																	</div>
																</div>
															</form:form>
															<br />
														</div>
													</div>
												</div>
											</div>
										</c:forEach>
									</table>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
	<script>
		$(function() {
			//Enable iCheck plugin for checkboxes
			//iCheck for checkbox and radio inputs
			$('.mailbox-messages input[type="checkbox"]').iCheck({
				checkboxClass : 'icheckbox_flat-blue',
				radioClass : 'iradio_flat-blue'
			});

			//Enable check and uncheck all functionality
			$(".checkbox-toggle").click(
					function() {
						var clicks = $(this).data('clicks');
						if (clicks) {
							//Uncheck all checkboxes
							$(".mailbox-messages input[type='checkbox']")
									.iCheck("uncheck");
							$(".fa", this).removeClass("fa-check-square-o")
									.addClass('fa-square-o');
						} else {
							//Check all checkboxes
							$(".mailbox-messages input[type='checkbox']")
									.iCheck("check");
							$(".fa", this).removeClass("fa-square-o").addClass(
									'fa-check-square-o');
						}
						$(this).data("clicks", !clicks);
					});

			//Handle starring for glyphicon and font awesome
			$(".mailbox-star").click(function(e) {
				e.preventDefault();
				//detect type
				var $this = $(this).find("a > i");
				var glyph = $this.hasClass("glyphicon");
				var fa = $this.hasClass("fa");

				//Switch states
				if (glyph) {
					$this.toggleClass("glyphicon-star");
					$this.toggleClass("glyphicon-star-empty");
				}

				if (fa) {
					$this.toggleClass("fa-star");
					$this.toggleClass("fa-star-o");
				}
			});
		});
	</script>
</body>
</html>