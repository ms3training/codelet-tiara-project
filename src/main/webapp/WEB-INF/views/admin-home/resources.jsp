<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Codelet</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport" />
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<header class="main-header">
		<a href="${pageContext.request.contextPath}/admin-home/" class="logo"> <span
			class="logo-mini"><b><i class="fa fa-code"
					aria-hidden="true"></i></b></span> <span class="logo-lg"><b>Codelet</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a id="lblAdminName">${firstName} ${lastName}</a></li>
					<li><a id="lblAdminLogout" href="/CodeletApplication/"><i
							class="fa fa-sign-out"></i></a></li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header text-center">ADMIN CONTROLS</li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/"> <i
						class="fa fa-dashboard"></i><span>Dashboard</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/forums"> <i
						class="fa fa-rss"></i><span>Forums</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/questions"> <i
						class="fa fa-question"></i><span>Questions</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/resources"> <i
						class="fa fa-files-o"></i><span>Resources</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/challenges"> <i
						class="fa fa-trophy"></i><span>Challenges</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/administrators"> <i
						class="fa fa-user"></i><span>Manage Administrators</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/settings"> <i
						class="fa fa-cogs"></i><span>Settings</span>
				</a></li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		<h1 class="text-center">
			<i class="fa fa-files-o"></i> Resources
		</h1>
		<br /> <br />
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Filter Resources</h2>
					</div>
					<div class="box-body center">
							<br />
				<form:form id="searchResourceForm" action="${pageContext.request.contextPath}/admin-home/resources/searchResources"
					method="post" modelAttribute="resource">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-8">
								<input type="text" class="form-control"
									placeholder="e.g. Spring Framework" name="title">
							</div>
							<div class="col-sm-4">
								<input name="submit" type="submit" value="Search"
									class="form-control btn btn-secondary" />
								<!-- <button class="form-control btn btn-secondary" type="submit">Search <i class="fa fa-search" aria-hidden="true"></i></button> -->
							</div>
						</div>
					</div>
				</form:form>
				<br />
				<div id="categories">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-2">
								<form:form id="gameDev" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control"
										value="Game Development" name="category">
									<button class="forum-category green-category" type="submit">
										<i class="fa fa-gamepad fa-3x" aria-hidden="true"></i><br />Game
										Development<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="database" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control" value="Database"
										name="category">
									<button class="forum-category lblue-category" type="submit">
										<i class="fa fa-database fa-3x" aria-hidden="true"></i><br />Database<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="mobileDev" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control"
										value="Mobile Development" name="category">
									<button class="forum-category pink-category" type="submit">
										<i class="fa fa-mobile fa-3x" aria-hidden="true"></i><br />Mobile
										Development<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="languages" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control" value="Languages"
										name="category">
									<button class="forum-category yellow-category" type="submit">
										<i class="fa fa-file-code-o fa-3x" aria-hidden="true"></i><br />Languages<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="testing" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control"
										value="Software Testing" name="category">
									<button class="forum-category teal-category" type="submit">
										<i class="fa fa-list-alt fa-3x" aria-hidden="true"></i><br />Software
										Testing<br />
									</button>
								</form:form>
							</div>
							<div class="col-sm-2">
								<form:form id="devTools" action="${pageContext.request.contextPath}/admin-home/resources/filterResourceCategory"
									method="post" modelAttribute="resource">
									<input type="hidden" class="form-control"
										value="Development Tools" name="category">
									<button class="forum-category purple-category" type="submit">
										<i class="fa fa-wrench fa-3x" aria-hidden="true"></i><br />Development
										Tools<br />
									</button>
								</form:form>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="box box-default collapsed-box">
					<div class="box-header with-border">
						<h6 class="box-title">
							<i class="fa fa-files-o" aria-hidden="true"></i> Add New Resource
						</h6>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-plus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<br />
						<form:form id="addResourceForm" action="${pageContext.request.contextPath}/admin-home/resources/addResource" method="post"
							modelAttribute="resource">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Title</label> <input type="text" name="title"
											id="title" class="form-control" required="required"
											placeholder="" />

									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-3 col-sm-offset-3">
										<label>Category</label> <select class="form-control"
											name="category">
											<option value="Game Development">Game Development</option>
											<option value="Database">Database</option>
											<option value="Mobile Development">Mobile
												Development</option>
											<option value="Languages">Languages</option>
											<option value="Software Testing">Software Testing</option>
											<option value="Development Tools">Development Tools</option>
										</select>
									</div>
									<div class="col-sm-3">
										<label>Type</label> <select class="form-control" name="type">
											<option value="Coding Challenge">Document</option>
											<option value="Interview question">Video</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>URL</label> <input type="text" name="url" id="url"
											class="form-control" required="required" placeholder="" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Description</label>
										<textarea class="form-control" rows="5" name="description">
										</textarea>
									</div>
								</div>
							</div>
							<div class="col-sm-2 col-sm-offset-3">
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
							</div>
						</form:form>
						<br /> <br /> <br /> <br />
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<br />
				</div>
			</div>
			</div>
			</div>
			
			
		
			
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Manage Resources</h2>
					</div>
					<div class="box-body center">
						<div>
							<table class="table table-responsive table-hover table-bordered">
								<tr>
									<th><h5>
											<b>Title</b>
										</h5></th>
									<th><h5>
											<b>Description</b>
										</h5></th>
									<th><h5>
											<b>Category</b>
										</h5></th>
									<th><h5>
											<b>Url</b>
										</h5></th>
									<th><h5>
											<b>Type</b>
										</h5></th>
									<th><h5>
											<b>Edit</b>
										</h5></th>
									<th><h5>
											<b>Delete</b>
										</h5></th>
								</tr>
								<c:forEach items="${resourceList}" var="resource">
									<tbody>
										<tr>
											<td>${resource.title}</td>
											<td>${resource.description}</td>
											<td>${resource.category}</td>
											<td>${resource.url}</td>
											<td>${resource.type}</td>
											<td><button type="button" class="btn btn-info btn-sm"
													data-toggle="modal"
													data-target="#editModal${resource.resourceID}">
													<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
												</button></td>
											<td><button type="button" class="btn btn-danger btn-sm"
													data-toggle="modal"
													data-target="#deleteModal${resource.resourceID}">
													<i class="fa fa-trash" aria-hidden="true"></i> DELETE
												</button></td>
										</tr>
									</tbody>
									<!-- DELETE -->
									<div class="modal fade" id="deleteModal${resource.resourceID}"
										tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header bgmainblue">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title text-center">Delete Resource?</h4>
												</div>
												<div class="modal-body text-center">
													<p>Are you sure you want to delete this resource?</p>
													<form:form id="deleteResourceForm" action="${pageContext.request.contextPath}/admin-home/resources/deleteResource" method="post"
														modelAttribute="resource">
														<input type="hidden" name="resourceID" id="resourceID"
															class="form-control" value="${resource.resourceID}"
															required="required" placeholder="" />
														<div class="form-group">
															<div class="row">
																<div class="col-sm-4">
																	<input name="submit" type="submit" value="Submit"
																		class="form-control btn btn-secondary" />
																</div>
																<div class="col-sm-4">
																	<button type="button"
																		class="form-control btn btn-default"
																		data-dismiss="modal">Cancel</button>
																</div>
															</div>
														</div>
													</form:form>
												</div>
											</div>
										</div>
									</div>
									<!-- EDIT -->
									<div class="modal fade" id="editModal${resource.resourceID}"
										tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header bgmainblue">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title text-center">Edit Resource</h4>
												</div>
												<div class="modal-body">
													<br />
													<form:form id="editResourceForm" action="${pageContext.request.contextPath}/admin-home/resources/editResource" method="post"
														modelAttribute="resource">
														<input type="hidden" name="resourceID" id="resourceID"
															class="form-control" value="${resource.resourceID}"
															required="required" placeholder="" />
														<div class="form-group">
															<div class="row">
																<div class="col-sm-6 col-sm-offset-3">
																	<label>Title</label> <input type="text" name="title"
																		id="title" class="form-control" required="required"
																		placeholder="" value="${resource.title}" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-3 col-sm-offset-3">
																	<label>Category</label> <select class="form-control"
																		name="category">
																		<option value="Game Development">Game
																			Development</option>
																		<option value="Database">Database</option>
																		<option value="Mobile Development">Mobile
																			Development</option>
																		<option value="Languages">Languages</option>
																		<option value="Software Testing">Software
																			Testing</option>
																		<option value="Development Tools">Development
																			Tools</option>
																	</select>
																</div>
																<div class="col-sm-3">
																	<label>Type</label> <select class="form-control"
																		name="type">
																		<option value="Coding Challenge">Document</option>
																		<option value="Interview question">Video</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-6 col-sm-offset-3">
																	<label>URL</label> <input type="text" name="url"
																		id="url" class="form-control" required="required"
																		placeholder="" value="${resource.url}" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-6 col-sm-offset-3">
																	<label>Description</label>
																	<textarea class="form-control" rows="5"
																		name="description" >${resource.description}</textarea>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-sm-4">
																	<input name="submit" type="submit" value="Submit"
																		class="form-control btn btn-secondary" />
																</div>
																<div class="col-sm-4">
																	<button type="button"
																		class="form-control btn btn-default"
																		data-dismiss="modal">Cancel</button>
																</div>
															</div>
														</div>
													</form:form>
													<br />
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
</body>
</html>