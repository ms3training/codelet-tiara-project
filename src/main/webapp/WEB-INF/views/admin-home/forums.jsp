<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Codelet</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport" />
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<header class="main-header">
		<a href="${pageContext.request.contextPath}/admin-home/" class="logo"> <span
			class="logo-mini"><b><i class="fa fa-code"
					aria-hidden="true"></i></b></span> <span class="logo-lg"><b>Codelet</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a id="lblAdminName">${firstName} ${lastName}</a></li>
					<li><a id="lblAdminLogout" href="/CodeletApplication/"><i class="fa fa-sign-out"></i></a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header text-center">ADMIN CONTROLS</li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/"> <i
						class="fa fa-dashboard"></i><span>Dashboard</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/forums"> <i
						class="fa fa-rss"></i><span>Forums</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/questions"> <i
						class="fa fa-question"></i><span>Questions</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/resources"> <i
						class="fa fa-files-o"></i><span>Resources</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/challenges"> <i
						class="fa fa-trophy"></i><span>Challenges</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/administrators"> <i
						class="fa fa-user"></i><span>Manage Administrators</span>
				</a></li>
				<li class="treeview"><a href="${pageContext.request.contextPath}/admin-home/settings"> <i
						class="fa fa-cogs"></i><span>Settings</span>
				</a></li>
			</ul>
		</section>
	</aside>
	<div class="content-wrapper">
		<h1 class="text-center">
			<i class="fa fa-rss"></i> Forums
		</h1>
		<br /> <br />
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Filter Forums</h2>
					</div>
					<div class="box-body center">
						<form:form id="searchForumForm" action="${pageContext.request.contextPath}/admin-home/forums/searchForum"
							method="post" modelAttribute="forum">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-8">
										<input type="text" class="form-control"
											placeholder="e.g. Spring Framework" name="title">
									</div>
									<div class="col-sm-4">
										<input name="submit" type="submit" value="Search"
											class="form-control btn btn-secondary" />
										<!-- <button class="form-control btn btn-secondary" type="submit">Search <i class="fa fa-search" aria-hidden="true"></i></button> -->
									</div>
								</div>
							</div>
						</form:form>
						<br />
						<div id="categories">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<form:form id="gameDev" action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory"
											method="post" modelAttribute="forum">
											<input type="hidden" class="form-control"
												value="Game Development" name="category">
											<button class="forum-category green-category" type="submit">
												<i class="fa fa-gamepad fa-3x" aria-hidden="true"></i><br />Game
												Development<br />
											</button>
										</form:form>
									</div>
									<div class="col-sm-2">
										<form:form id="database"
											action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory" method="post"
											modelAttribute="forum">
											<input type="hidden" class="form-control" value="Database"
												name="category">
											<button class="forum-category lblue-category" type="submit">
												<i class="fa fa-database fa-3x" aria-hidden="true"></i><br />Database<br />
											</button>
										</form:form>
									</div>
									<div class="col-sm-2">
										<form:form id="mobileDev"
											action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory" method="post"
											modelAttribute="forum">
											<input type="hidden" class="form-control"
												value="Mobile Development" name="category">
											<button class="forum-category pink-category" type="submit">
												<i class="fa fa-mobile fa-3x" aria-hidden="true"></i><br />Mobile
												Development<br />
											</button>
										</form:form>
									</div>
									<div class="col-sm-2">
										<form:form id="languages"
											action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory" method="post"
											modelAttribute="forum">
											<input type="hidden" class="form-control" value="Languages"
												name="category">
											<button class="forum-category yellow-category" type="submit">
												<i class="fa fa-file-code-o fa-3x" aria-hidden="true"></i><br />Languages<br />
											</button>
										</form:form>
									</div>
									<div class="col-sm-2">
										<form:form id="testing" action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory"
											method="post" modelAttribute="forum">
											<input type="hidden" class="form-control"
												value="Software Testing" name="category">
											<button class="forum-category teal-category" type="submit">
												<i class="fa fa-list-alt fa-3x" aria-hidden="true"></i><br />Software
												Testing<br />
											</button>
										</form:form>
									</div>
									<div class="col-sm-2">
										<form:form id="devTools"
											action="${pageContext.request.contextPath}/admin-home/forums/filterForumCategory" method="post"
											modelAttribute="forum">
											<input type="hidden" class="form-control"
												value="Development Tools" name="category">
											<button class="forum-category purple-category" type="submit">
												<i class="fa fa-wrench fa-3x" aria-hidden="true"></i><br />Development
												Tools<br />
											</button>
										</form:form>
									</div>
								</div>
							</div>
						</div>
							<br />
				<div class="box box-default collapsed-box">
					<div class="box-header with-border">
						<h6 class="box-title">
							<i class="fa fa-rss" aria-hidden="true"></i>  Add New
							Forum
						</h6>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-plus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form:form id="formForm" action="${pageContext.request.contextPath}/admin-home/forums/addForum" method="post"
							modelAttribute="forum">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Title</label>
										<input
												type="text" name="title" id="title" class="form-control"
												required="required" placeholder="" />
										
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Category</label> <select class="form-control"
											name="category">
											<option value="Game Development">Game Development</option>
											<option value="Database">Database</option>
											<option value="Mobile Development">Mobile
												Development</option>
											<option value="Languages">Languages</option>
											<option value="Software Testing">Software Testing</option>
											<option value="Development Tools">Development Tools</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<label>Description</label>
										<textarea class="form-control" rows="5" name="description">
										</textarea>
									</div>
								</div>
							</div>
							<div class="col-sm-2 col-sm-offset-3">
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
							</div>
						</form:form>
						<br /> <br /><br />
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="box blue-top">
					<div class="box-header with-border">
						<h2 class="box-title">Manage Forums</h2>
					</div>
					<div class="box-body center">
						<div>
			<table class="table table-responsive table-hover table-bordered">
					<tr>
						<th><h5><b>Title</b></h5></th>
						<th><h5><b>Description</b></h5></th>
						<th><h5><b>Category</b></h5></th>
						<th><h5><b>Edit</b></h5></th>
						<th><h5><b>Delete</b></h5></th>
					</tr>
				<c:forEach items="${forumList}" var="forum">
					<tbody>
						<tr>
							<td>${forum.title}</td>
							<td>${forum.description}</td>
							<td>${forum.category}</td>
							<td><button type="button" class="btn btn-info btn-sm"
									data-toggle="modal" data-target="#editModal${forum.forumID}">
									<i class="fa fa-pencil" aria-hidden="true"></i> EDIT
								</button></td>
							<td><button type="button" class="btn btn-danger btn-sm"
									data-toggle="modal" data-target="#deleteModal${forum.forumID}">
									<i class="fa fa-trash" aria-hidden="true"></i> DELETE
								</button></td>
						</tr>
					</tbody>
					<!-- DELETE -->
					<div class="modal fade" id="deleteModal${forum.forumID}" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header bgmainblue">
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title text-center">Delete Forum?</h4>
								</div>
								<div class="modal-body text-center">
									<p>Are you sure you want to delete this forum?</p>
									<form:form id="forumForm" action="" method="post"
										modelAttribute="question">
										<input type="hidden" name="forumID" id="forumID"
											class="form-control" value="${forum.forumID}"
											required="required" placeholder="" />
										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<input name="submit" type="submit" value="Submit"
														class="form-control btn btn-secondary" />
												</div>
												<div class="col-sm-4">
													<button type="button" class="form-control btn btn-default"
														data-dismiss="modal">Cancel</button>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
					<!-- EDIT -->
					<div class="modal fade" id="editModal${forum.forumID}" tabindex="-1" role="dialog"
						aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header bgmainblue">
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title text-center">Edit Forum</h4>
								</div>
								<div class="modal-body">
									<br />
									<form:form id="questionForm" action="" method="post"
										modelAttribute="question">

										<input type="hidden" name="forumID" id="forumID"
											class="form-control" value="${forum.forumID}"
											required="required" placeholder="" />

										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label>Title</label> <input type="text" name="title"
														id="title" class="form-control" required="required"
														placeholder="" value="${forum.title}" />

												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label>Category</label> <select class="form-control"
														name="category">
														<option value="Game Development">Game Development</option>
														<option value="Database">Database</option>
														<option value="Mobile Development">Mobile
															Development</option>
														<option value="Languages">Languages</option>
														<option value="Software Testing">Software Testing</option>
														<option value="Development Tools">Development
															Tools</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<label>Description</label>
													<textarea class="form-control" rows="5" name="description">${forum.description}</textarea>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-4">
													<input name="submit" type="submit" value="Submit"
														class="form-control btn btn-secondary" />
												</div>
												<div class="col-sm-4">
													<button type="button" class="form-control btn btn-default"
														data-dismiss="modal">Cancel</button>
												</div>
											</div>
										</div>
									</form:form>
									<br />
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</table>
		</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<spring:url value="/resources/js/app.min.js" var="appJs" />
	<script src="${appJs}"></script>
	<script>
	  $(function () {
	    //Enable iCheck plugin for checkboxes
	    //iCheck for checkbox and radio inputs
	    $('.mailbox-messages input[type="checkbox"]').iCheck({
	      checkboxClass: 'icheckbox_flat-blue',
	      radioClass: 'iradio_flat-blue'
	    });

	    //Enable check and uncheck all functionality
	    $(".checkbox-toggle").click(function () {
	      var clicks = $(this).data('clicks');
	      if (clicks) {
	        //Uncheck all checkboxes
	        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
	        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
	      } else {
	        //Check all checkboxes
	        $(".mailbox-messages input[type='checkbox']").iCheck("check");
	        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
	      }
	      $(this).data("clicks", !clicks);
	    });

	    //Handle starring for glyphicon and font awesome
	    $(".mailbox-star").click(function (e) {
	      e.preventDefault();
	      //detect type
	      var $this = $(this).find("a > i");
	      var glyph = $this.hasClass("glyphicon");
	      var fa = $this.hasClass("fa");

	      //Switch states
	      if (glyph) {
	        $this.toggleClass("glyphicon-star");
	        $this.toggleClass("glyphicon-star-empty");
	      }

	      if (fa) {
	        $this.toggleClass("fa-star");
	        $this.toggleClass("fa-star-o");
	      }
	    });
	  });
	</script>
</body>
</html>