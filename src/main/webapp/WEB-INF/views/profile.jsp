<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Codelet</title>
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="html codelet-bg">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/CodeletApplication/"><i
					class="fa fa-code fa-2x" aria-hidden="true"></i> <span
					style="font-size: 1.7em;">Codelet</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right nav-pills">
					<li><a class="btn btn-secondary btn-home" href="login">Login</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="spacer"></div>
	<div class="container">
		<div class="login col-sm-6 col-sm-offset-3" style="height:490px !important">
			<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#profile"
						aria-controls="profile" role="tab" data-toggle="tab">Create
							Profile</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="profile">
						<br />
						<form:form id="profileForm" action="profile" method="post"
							modelAttribute="user">
							 <input type="hidden" value="${userID}" type="hidden" name="userID"/>						
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Code Level</label> 
										<select id="avatar" class="form-control" name="codeLevel">
										  <option value="" disabled selected style="display:none;">Select</option>
										  <option value="Newbie">Newbie</option>
											<option value="Intermediate">Intermediate</option>
											<option value="Guru">Guru</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Bio</label>
										<textarea class="form-control" rows="5" name="bio">
										</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
									</div>
								</div>
							</div>
							
									
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>




