package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tiarajarvis.codelet.model.Forum;

@Repository
public class ForumDaoImpl implements ForumDao{

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Forum> listAllForums() {
		String sqlQuery = "SELECT * FROM forums";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Forum> forumList = namedParameterJdbcTemplate.query(sqlQuery, params, new ForumRowMapper());
		return forumList;
	}

	public List<Forum> listForumsByCategory(String category) {
		String sqlQuery = "SELECT * FROM forums WHERE category = :category";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("category", category);
		List<Forum> forumList = namedParameterJdbcTemplate.query(sqlQuery, params, new ForumRowMapper());
		return forumList;
	}
	
	public Forum findForumById(int id) {
		SqlParameterSource params = new MapSqlParameterSource("forumID", id);
		String sqlQuery = "SELECT * FROM forums WHERE forumID = :forumID";
		Forum forum = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new ForumRowMapper());
		return forum;
	}
	
	public List<Forum> searchForums(String searchQuery) {
		String sqlQuery = "SELECT * FROM forums WHERE title LIKE :title";
		String query = "%" + searchQuery.toLowerCase().trim() + "%";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("title", query);
		List<Forum> forumList = namedParameterJdbcTemplate.query(sqlQuery, params, new ForumRowMapper());
		return forumList;
	}

	public int addForum(Forum forum) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(forum);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO forums (title, description, category) "
				+ "VALUES(:title, :description, :category)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"forumID"});
		return keyHolder.getKey().intValue();
	}

	public boolean updateForum(Forum forum) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("forumID", forum.getForumID());
		params.addValue("title", forum.getTitle());
		params.addValue("description", forum.getDescription());
		params.addValue("category", forum.getCategory());
		String sqlQuery = "UPDATE forums SET title = :title, description = :description, category = :category WHERE forumID = :forumID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public boolean deleteForum(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("forumID", id);
		String sqlQuery = "DELETE FROM forums WHERE forumID = :forumID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public Forum findForumByTitle(String title) {
		SqlParameterSource params = new MapSqlParameterSource("title", title);
		String sqlQuery = "SELECT * FROM forums WHERE title = :title";
		Forum forum = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new ForumRowMapper());
		return forum;
	}
	
	private static final class ForumRowMapper implements RowMapper<Forum> {

		public Forum mapRow(ResultSet rs, int rowNum) throws SQLException {
			Forum forum = new Forum();
			forum.setForumID(rs.getInt("forumID"));
			forum.setTitle(rs.getString("title"));
			forum.setDescription(rs.getString("description"));
			forum.setCategory(rs.getString("category"));
			return forum;
		}
	}
}
