package com.tiarajarvis.codelet.dao;

import java.util.List;

import com.tiarajarvis.codelet.model.Forum;

public interface ForumDao {
	public List<Forum> listAllForums();
	public Forum findForumById(int id);
	public Forum findForumByTitle(String title);
	public int addForum(Forum forum);
	public boolean updateForum(Forum forum);
	public boolean deleteForum(int id);
	public List<Forum> listForumsByCategory(String category);
	public List<Forum> searchForums(String searchQuery);
}
