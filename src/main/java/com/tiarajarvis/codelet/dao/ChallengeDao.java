package com.tiarajarvis.codelet.dao;

import java.util.List;

import com.tiarajarvis.codelet.model.Challenge;

public interface ChallengeDao {
	public List<Challenge> listAllChallenges();
	public List<Challenge> listChallengesByTitle(String searchQuery);
	public List<Challenge> listChallengesByCategory(String category);
	public int addChallenge(Challenge challenge);
	public boolean updateChallenge(Challenge challenge);
	public boolean deleteChallenge(int id);
	public Challenge findChallengeById(int id);
}
