package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tiarajarvis.codelet.model.Response;

@Repository
public class ResponseDaoImpl implements ResponseDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public int addResponse(Response response) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(response);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO responses (userID, questionID, content, likes, postDate) "
				+ "VALUES(:userID, :questionID, :content, :likes, :postDate)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"responseID"});
		return keyHolder.getKey().intValue();
	}

	public List<Response> listResponsesByQuestionId(int id) throws EmptyResultDataAccessException {
		List<Response> responseList;
		SqlParameterSource params = new MapSqlParameterSource("questionID", id);
		try {
			String sqlQuery = "SELECT * FROM responses WHERE questionID = :questionID";
			responseList = namedParameterJdbcTemplate.query(sqlQuery, params, new ResponseRowMapper());
		} catch (EmptyResultDataAccessException ex) {
			responseList = null;
		}		
		return responseList;
	}
	
	private static final class ResponseRowMapper implements RowMapper<Response> {

		public Response mapRow(ResultSet rs, int rowNum) throws SQLException {
			Response response = new Response();
			response.setUserID(rs.getInt("userID"));
			response.setQuestionID(rs.getInt("questionID"));
			response.setContent(rs.getString("content"));
			response.setLikes(rs.getInt("likes"));
			response.setPostDate(rs.getString("postDate"));
			return response;
		}
	}
}
