package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tiarajarvis.codelet.model.FavoritedQuestion;
import com.tiarajarvis.codelet.model.Question;

@Repository
public class QuestionDaoImpl implements QuestionDao{

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Question> listQuestionsByUserID(int id) {
		SqlParameterSource params = new MapSqlParameterSource("userID", id);
		String sqlQuery = "SELECT * FROM questions WHERE userID = :userID";
		List<Question> questionList = namedParameterJdbcTemplate.query(sqlQuery, params, new QuestionRowMapper());
		return questionList;
	}

	public int addQuestion(Question question) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(question);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO questions (userID, forumID, title, content, tags, likes, flagged, postDate) "
				+ "VALUES(:userID, :forumID, :title, :content, :tags, :likes, :flagged, :postDate)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"questionID"});
		return keyHolder.getKey().intValue();
	}

	public boolean updateQuestion(Question question) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("questionID", question.getQuestionID());
		params.addValue("title", question.getTitle());
		params.addValue("content", question.getContent());
		String sqlQuery = "UPDATE questions SET title = :title, content = :content WHERE questionID = :questionID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public boolean deleteQuestion(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("questionID", id);
		String sqlQuery = "DELETE FROM questions WHERE questionID = :questionID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public List<Question> listQuestionsByForumID(int id) {
		SqlParameterSource params = new MapSqlParameterSource("forumID", id);
		String sqlQuery = "SELECT * FROM questions WHERE forumID = :forumID";
		List<Question> questionList = namedParameterJdbcTemplate.query(sqlQuery, params, new QuestionRowMapper());
		return questionList;
	}
	
	public Question findQuestionById(int id) {
		SqlParameterSource params = new MapSqlParameterSource("questionID", id);
		String sqlQuery = "SELECT * FROM questions WHERE questionID = :questionID";
		Question question = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new QuestionRowMapper());
		return question;
	}
	
	public List<Question> listAllQuestions() {
		String sqlQuery = "SELECT * FROM questions";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Question> questionList = namedParameterJdbcTemplate.query(sqlQuery, params, new QuestionRowMapper());
		return questionList;
	}
	
	public List<Question> listFlaggedQuestions() {
		String sqlQuery = "SELECT * FROM questions WHERE flagged = 'Yes'";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Question> questionList = namedParameterJdbcTemplate.query(sqlQuery, params, new QuestionRowMapper());
		return questionList;
	}
	
	public List<Question> listQuestionsByTitle(String searchQuery) {
		String sqlQuery = "SELECT * FROM questions WHERE title LIKE :title";
		String query = "%" + searchQuery.toLowerCase().trim() + "%";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("title", query);
		List<Question> questionList = namedParameterJdbcTemplate.query(sqlQuery, params, new QuestionRowMapper());
		return questionList;
	}
	
	private static final class QuestionRowMapper implements RowMapper<Question> {

		public Question mapRow(ResultSet rs, int rowNum) throws SQLException {
			Question question = new Question();
			question.setForumID(rs.getInt("forumID"));
			question.setUserID(rs.getInt("userID"));
			question.setQuestionID(rs.getInt("questionID"));
			question.setTitle(rs.getString("title"));
			question.setContent(rs.getString("content"));
			//question.setTags(rs.getArray("content"));
			question.setLikes(rs.getInt("likes"));
			question.setFlagged(rs.getString("flagged"));
			question.setPostDate(rs.getString("postDate"));
			return question;
		}
	}
	
	private static final class FavoriteRowMapper implements RowMapper<FavoritedQuestion> {

		public FavoritedQuestion mapRow(ResultSet rs, int rowNum) throws SQLException {
			FavoritedQuestion question = new FavoritedQuestion();
			question.setForumID(rs.getInt("forumID"));
			question.setUserID(rs.getInt("userID"));
			question.setQuestionID(rs.getInt("questionID"));
			question.setTitle(rs.getString("title"));
			question.setContent(rs.getString("content"));
			//question.setTags(rs.getArray("content"));
			question.setLikes(rs.getInt("likes"));
			question.setFlagged(rs.getString("flagged"));
			question.setPostDate(rs.getString("postDate"));
			question.setFavoriteID(rs.getInt("favoriteID"));
			question.setUserID(rs.getInt("userID"));
			question.setQuestionID(rs.getInt("questionID"));
			return question;
		}
	}
	
	public void flagQuestion(int questionID, String flagged) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("flagged", flagged);
		params.addValue("questionID", questionID);
		String sqlQuery = "UPDATE questions SET flagged = :flagged WHERE questionID = :questionID";	
		namedParameterJdbcTemplate.update(sqlQuery, params);
	}

	public void likeQuestion(int likes, int questionID) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("likes", likes);
		params.addValue("questionID", questionID);
		String sqlQuery = "UPDATE questions SET likes = :likes WHERE questionID = :questionID";	
		namedParameterJdbcTemplate.update(sqlQuery, params);
	}

	public void favoriteQuestion(int userID, int questionID) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userID", userID);
		params.addValue("questionID", questionID);
		String sqlQuery = "INSERT INTO favorites (userID, questionID) VALUES(:userID, :questionID)";	
		namedParameterJdbcTemplate.update(sqlQuery, params);
	}

	public List<FavoritedQuestion> listFavoriteQuestionsByID(int userID) {
		List<FavoritedQuestion> favoritesList = new ArrayList<FavoritedQuestion>();
		String sqlQuery = "SELECT * FROM questions as q INNER JOIN favorites as f ON q.questionID = f.questionID";
		List<FavoritedQuestion> resultList = namedParameterJdbcTemplate.query(sqlQuery, new FavoriteRowMapper());	
		try {
			for (FavoritedQuestion favoritedQuestion : resultList) {
				if (favoritedQuestion.getUserID() == userID) {
					favoritesList.add(favoritedQuestion);
				}
			}
			return favoritesList;
		} catch (Exception e) {
			return null;
		}
	}
}	
