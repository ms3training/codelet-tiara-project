package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.tiarajarvis.codelet.model.Resource;

public class ResourceDaoImpl implements ResourceDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Resource> listAllResources() {
		String sqlQuery = "SELECT * FROM resources";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Resource> resourceList = namedParameterJdbcTemplate.query(sqlQuery, params, new ResourceRowMapper());
		return resourceList;
	}

	public int addResource(Resource resource) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(resource);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO resources (userID, title, category, type, description, url) "
				+ "VALUES(:userID, :title, :category, :type, :description, :url)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"challengeID"});
		return keyHolder.getKey().intValue();
	}

	public boolean updateResource(Resource resource) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("resourceID", resource.getResourceID());
		params.addValue("title", resource.getTitle());
		params.addValue("description", resource.getDescription());
		params.addValue("category", resource.getCategory());
		params.addValue("url", resource.getUrl());
		params.addValue("type", resource.getType());
		String sqlQuery = "UPDATE resources SET title = :title, description = :description, category = :category, url = :url, type = :type"
				+ " WHERE resourceID = :resourceID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public boolean deleteResource(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("resourceID", id);
		String sqlQuery = "DELETE FROM resources WHERE resourceID = :resourceID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public Resource findResourceById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Resource> listResourcesByCategory(String category) {
		String sqlQuery = "SELECT * FROM resources WHERE category = :category";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("category", category);
		List<Resource> resourceList = namedParameterJdbcTemplate.query(sqlQuery, params, new ResourceRowMapper());
		return resourceList;
	}

	public List<Resource> listResourcesByTitle(String searchQuery) {
		String sqlQuery = "SELECT * FROM resources WHERE title LIKE :title";
		String query = "%" + searchQuery.toLowerCase().trim() + "%";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("title", query);
		List<Resource> resourceList = namedParameterJdbcTemplate.query(sqlQuery, params, new ResourceRowMapper());
		return resourceList;
	}
	
	private static final class ResourceRowMapper implements RowMapper<Resource> {

		public Resource mapRow(ResultSet rs, int rowNum) throws SQLException {
			Resource resource = new Resource();
			resource.setResourceID(rs.getInt("resourceID"));
			resource.setTitle(rs.getString("title"));
			resource.setCategory(rs.getString("category"));
			resource.setType(rs.getString("type"));
			resource.setDescription(rs.getString("description"));
			resource.setUrl(rs.getString("url"));
			return resource;
		}
	}
}
