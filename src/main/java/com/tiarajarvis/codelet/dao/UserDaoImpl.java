package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.jasper.tagplugins.jstl.core.If;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.object.RdbmsOperation;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;


import com.tiarajarvis.codelet.model.Question;
import com.tiarajarvis.codelet.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public int addNewUser(User user) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(user);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO users (userRole, firstName, lastName, email, password, picture, codeLevel, bio, accountStatus) "
				+ "VALUES(:userRole, :firstName, :lastName, :email, :password, :picture, :codeLevel, :bio, :accountStatus)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"userID"});
		return keyHolder.getKey().intValue();
	}

	public boolean updateUser(int id,  User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userID", id);
		params.addValue("user", user);
		String sqlQuery = "UPDATE users SET userRole = :userRole, firstName = :firstName, lastName = :lastName, email = :email, "
				+ "				password = :password, picture = :picture, codeLevel = :codeLevel, bio = :bio, accountStatus = :accountStatus WHERE userID = :userID";
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}
	
	public boolean createUserProfile(int id,  User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userID", id);
		params.addValue("codeLevel", user.getCodeLevel());
		params.addValue("bio", user.getBio());
		String sqlQuery = "UPDATE users SET codeLevel = :codeLevel, bio = :bio WHERE userID = :userID";
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}
	
	public boolean deleteUser(int id) {
		SqlParameterSource beanParams = new MapSqlParameterSource("id", id);
		String sqlQuery = "DELETE FROM users WHERE userID = :id";
		return namedParameterJdbcTemplate.update(sqlQuery, beanParams) == 1;
	}

	public User findUserById(int id) {
		SqlParameterSource params = new MapSqlParameterSource("userID", id);
		String sqlQuery = "SELECT * FROM users WHERE userID = :userID";
		User user = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new UserRowMapper());
		return user;
	}

	public User findUserByEmail(String email) {
		SqlParameterSource params = new MapSqlParameterSource("email", email);
		String sqlQuery = "SELECT * FROM users WHERE email = :email";
		User user = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new UserRowMapper());
		return user;
	}
	
	public User validateUserLogin(String email, String password) throws EmptyResultDataAccessException {
		User user;
		try {
			user = findUserByEmail(email);
			if(BCrypt.checkpw(password, user.getPassword())){
				return user;
			}
			else{
				return null;
			}
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	private static final class UserRowMapper implements RowMapper<User> {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setUserID(rs.getInt("userID"));
			user.setUserRoleID(rs.getString("userRole"));
			user.setFirstName(rs.getString("firstname"));
			user.setLastName(rs.getString("lastname"));
			user.setEmail(rs.getString("email"));
			user.setPassword(rs.getString("password"));
			user.setPicture(rs.getBlob("picture"));
			user.setCodeLevel(rs.getString("codeLevel"));
			user.setBio(rs.getString("bio"));
			user.setAccountStatus(rs.getString("accountStatus"));
			return user;
		}
	}

	public boolean editProfile(int userID, User user) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userID", userID);
		params.addValue("firstName", user.getFirstName());
		params.addValue("lastName", user.getLastName());
		params.addValue("picture", user.getPicture());
		params.addValue("codeLevel", user.getCodeLevel());
		params.addValue("bio", user.getBio());
		String sqlQuery = "UPDATE users SET firstName = :firstName, lastName = :lastName, picture = :picture, codeLevel = :codeLevel, bio = :bio WHERE userID = :userID";
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public List<User> listAdmins() {
		String sqlQuery = "SELECT * FROM users WHERE userRole = 'admin'";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<User> userList = namedParameterJdbcTemplate.query(sqlQuery, params, new UserRowMapper());
		return userList;
	}
}
