package com.tiarajarvis.codelet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.tiarajarvis.codelet.model.Challenge;

public class ChallengeDaoImpl implements ChallengeDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Challenge> listAllChallenges() {
		String sqlQuery = "SELECT * FROM challenges";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Challenge> challengeList = namedParameterJdbcTemplate.query(sqlQuery, params, new ChallengeRowMapper());
		return challengeList;
	}

	public int addChallenge(Challenge challenge) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(challenge);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sqlQuery = "INSERT INTO challenges (userID, title, category, type, question, answer) "
				+ "VALUES(:userID, :title, :category, :type, :question, :answer)";
		 
		namedParameterJdbcTemplate.update(sqlQuery, beanParams, keyHolder, new String[]{"challengeID"});
		return keyHolder.getKey().intValue();
	}

	public boolean updateChallenge(Challenge challenge) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("challengeID", challenge.getChallengeID());
		params.addValue("title", challenge.getTitle());
		params.addValue("question", challenge.getQuestion());
		params.addValue("answer", challenge.getAnswer());
		params.addValue("category", challenge.getCategory());
		String sqlQuery = "UPDATE challenges SET title = :title, question = :question, answer = :answer, category = :category  WHERE challengeID = :challengeID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public boolean deleteChallenge(int challengeID) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("challengeID", challengeID);
		String sqlQuery = "DELETE FROM challenges WHERE challengeID = :challengeID";	
		return namedParameterJdbcTemplate.update(sqlQuery, params) == 1;
	}

	public Challenge findChallengeById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Challenge> listChallengesByTitle(String searchQuery) {
		String sqlQuery = "SELECT * FROM challenges WHERE title LIKE :title";
		String query = "%" + searchQuery.toLowerCase().trim() + "%";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("title", query);
		List<Challenge> challengeList = namedParameterJdbcTemplate.query(sqlQuery, params, new ChallengeRowMapper());
		return challengeList;
	}

	public List<Challenge> listChallengesByCategory(String category) {
		String sqlQuery = "SELECT * FROM challenges WHERE category = :category";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("category", category);
		List<Challenge> challengeList = namedParameterJdbcTemplate.query(sqlQuery, params, new ChallengeRowMapper());
		return challengeList;
	}
	
	private static final class ChallengeRowMapper implements RowMapper<Challenge> {

		public Challenge mapRow(ResultSet rs, int rowNum) throws SQLException {
			Challenge challenge = new Challenge();
			challenge.setUserID(rs.getInt("userID"));
			challenge.setChallengeID(rs.getInt("challengeID"));
			challenge.setTitle(rs.getString("title"));
			challenge.setCategory(rs.getString("category"));
			challenge.setType(rs.getString("type"));
			challenge.setQuestion(rs.getString("question"));
			challenge.setAnswer(rs.getString("answer"));
			return challenge;
		}
	}
}
