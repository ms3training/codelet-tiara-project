package com.tiarajarvis.codelet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tiarajarvis.codelet.dao.UserDao;
import com.tiarajarvis.codelet.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	UserDao userDao;
	private static final String REGULAR = "regular";
	private static final String ACTIVATE = "active";
	
	//@Autowired
	//private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public int addNewUser(User user) {
		if(user.getUserRole() == null){
			user.setUserRoleID(REGULAR);
		}
		user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)));
		user.setAccountStatus(ACTIVATE);
		return userDao.addNewUser(user);
	}

	public boolean updateUser(int id, User user) {
		return userDao.updateUser(id, user);
	}

	public boolean createUserProfile(int id, User user) {
		return userDao.createUserProfile(id, user);
	}
	
	public boolean deleteUser(int id) {
		return userDao.deleteUser(id);
	}

	public User findUserById(int id) {
		return userDao.findUserById(id);
	}
	
	public User findUserByEmail(String email) {
		return userDao.findUserByEmail(email);
	}
	
	public User validateUserLogin(String email, String password) {
		return userDao.validateUserLogin(email, password);
	}

	public boolean editProfile(int id, User user) {
		user.setPicture(null);
		return userDao.editProfile(id, user);
	}

	public List<User> listAdmins() {
		return userDao.listAdmins();
	}
}
