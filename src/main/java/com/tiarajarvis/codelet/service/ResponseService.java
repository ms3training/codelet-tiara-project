package com.tiarajarvis.codelet.service;

import java.util.List;

import com.tiarajarvis.codelet.model.Response;

public interface ResponseService {
	public int addResponse(Response response);
	public List<Response> listResponsesByQuestionId(int id);
}
