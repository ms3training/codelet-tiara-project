package com.tiarajarvis.codelet.service;

import java.util.List;

import com.tiarajarvis.codelet.model.Resource;

public interface ResourceService {
	public List<Resource> listAllResources();
	public List<Resource> listResourcesByCategory(String category);
	public List<Resource> listResourcesByTitle(String searchQuery);
	public int addResource(Resource resource);
	public boolean updateResource(Resource resource);
	public boolean deleteResource(int id);
	public Resource findResourceById(int id);
}
