package com.tiarajarvis.codelet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.tiarajarvis.codelet.dao.ForumDao;
import com.tiarajarvis.codelet.model.Forum;

import junit.swingui.TestHierarchyRunView;

public class ForumServiceImpl implements ForumService{

	ForumDao forumDao;
	
	@Autowired
	public void setForumDao(ForumDao forumDao) {
		this.forumDao = forumDao;
	}

	public List<Forum> listAllForums() {
		return forumDao.listAllForums();
	}

	public List<Forum> listForumsByCategory(String category) {
		return forumDao.listForumsByCategory(category);
	}

	public List<Forum> searchForums(String searchQuery) {
		return forumDao.searchForums(searchQuery);
	}

	public Forum findForumById(int id) {
		return forumDao.findForumById(id);
	}

	public Forum findForumByTitle(String title) {
		return forumDao.findForumByTitle(title);
	}

	public int addForum(Forum forum) {
		return forumDao.addForum(forum);
	}

	public boolean updateForum(Forum forum) {
		return forumDao.updateForum(forum);
	}

	public boolean deleteForum(int id) {
		return forumDao.deleteForum(id);
	}

}
