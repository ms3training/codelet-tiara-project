package com.tiarajarvis.codelet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.tiarajarvis.codelet.dao.ResourceDao;
import com.tiarajarvis.codelet.model.Resource;

public class ResourceServiceImpl implements ResourceService {

	ResourceDao resourceDao;
	
	@Autowired
	public void setResourceDao(ResourceDao resourceDao) {
		this.resourceDao = resourceDao;
	}
	
	public List<Resource> listAllResources() {
		return resourceDao.listAllResources();
	}

	public int addResource(Resource resource) {
		return resourceDao.addResource(resource);
	}

	public boolean updateResource(Resource resource) {
		return resourceDao.updateResource(resource);
	}

	public boolean deleteResource(int id) {
		return resourceDao.deleteResource(id);
	}

	public Resource findResourceById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Resource> listResourcesByCategory(String category) {
		return resourceDao.listResourcesByCategory(category);
	}

	public List<Resource> listResourcesByTitle(String searchQuery) {
		return resourceDao.listResourcesByTitle(searchQuery);
	}

}
