package com.tiarajarvis.codelet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.tiarajarvis.codelet.dao.ChallengeDao;
import com.tiarajarvis.codelet.model.Challenge;

public class ChallengeServiceImpl implements ChallengeService {

	ChallengeDao challengeDao;
	
	@Autowired
	public void setChallengeDao(ChallengeDao challengeDao) {
		this.challengeDao = challengeDao;
	}

	public List<Challenge> listAllChallenges() {
		return challengeDao.listAllChallenges();
	}

	public int addChallenge(Challenge challenge) {
		return challengeDao.addChallenge(challenge);
	}

	public boolean updateChallenge(Challenge challenge) {
		return challengeDao.updateChallenge(challenge);
	}

	public boolean deleteChallenge(int id) {
		return challengeDao.deleteChallenge(id);
	}

	public Challenge findChallengeById(int id) {
		return challengeDao.findChallengeById(id);
	}

	public List<Challenge> listChallengesByTitle(String searchQuery) {
		return challengeDao.listChallengesByTitle(searchQuery);
	}

	public List<Challenge> listChallengesByCategory(String category) {
		return challengeDao.listChallengesByCategory(category);
	}

}
