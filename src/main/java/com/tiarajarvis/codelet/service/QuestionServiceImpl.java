package com.tiarajarvis.codelet.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiarajarvis.codelet.dao.QuestionDao;
import com.tiarajarvis.codelet.model.FavoritedQuestion;
import com.tiarajarvis.codelet.model.Question;

@Service
public class QuestionServiceImpl implements QuestionService {

	QuestionDao questionDao;
	
	@Autowired
	public void setQuestionDao(QuestionDao questionDao) {
		this.questionDao = questionDao;
	}
	
	public List<Question> listQuestionsByUserID(int id) {
		return questionDao.listQuestionsByUserID(id);
	}
	
	public List<Question> listQuestionsByForumID(int id) {
		return questionDao.listQuestionsByForumID(id);
	}
	
	public int addQuestion(Question question) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
		Date date = new Date();
		question.setPostDate(dateFormat.format(date));
		question.setLikes(0);
		question.setTags(null);
		question.setFlagged("No");
		return questionDao.addQuestion(question);
	}

	public boolean updateQuestion(Question question) {
		return questionDao.updateQuestion(question);
	}

	public boolean deleteQuestion(int id) {
		return questionDao.deleteQuestion(id);
	}

	public Question findQuestionById(int id) {
		return questionDao.findQuestionById(id);
	}

	public List<Question> listAllQuestions() {
		return questionDao.listAllQuestions();
	}

	public List<Question> listFlaggedQuestions() {
		return questionDao.listFlaggedQuestions();
	}

	public List<Question> listQuestionsByTitle(String searchQuery) {
		return questionDao.listQuestionsByTitle(searchQuery);
	}

	public void flagQuestion(int questionID, String flagged) {
		questionDao.flagQuestion(questionID, flagged);
	}

	public void likeQuestion(int like, int questionID) {
		questionDao.likeQuestion(like, questionID);
	}

	public void favoriteQuestion(int userID, int questionID) {
		questionDao.favoriteQuestion(userID, questionID);	
	}

	public List<FavoritedQuestion> listFavoriteQuestionsByID(int id) {
		return questionDao.listFavoriteQuestionsByID(id);
	}
}
