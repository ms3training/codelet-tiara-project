package com.tiarajarvis.codelet.service;

import java.util.List;

import com.tiarajarvis.codelet.model.Challenge;

public interface ChallengeService {
	public List<Challenge> listAllChallenges();
	public List<Challenge> listChallengesByTitle(String searchQuery);
	public List<Challenge> listChallengesByCategory(String category);
	public int addChallenge(Challenge challenge);
	public boolean updateChallenge(Challenge challenge);
	public boolean deleteChallenge(int id);
	public Challenge findChallengeById(int id);
}
