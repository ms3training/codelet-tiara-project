package com.tiarajarvis.codelet.service;

import java.util.List;

import com.tiarajarvis.codelet.model.FavoritedQuestion;
import com.tiarajarvis.codelet.model.Question;

public interface QuestionService {
	public List<Question> listQuestionsByUserID(int id);
	public List<FavoritedQuestion> listFavoriteQuestionsByID(int id);
	public List<Question> listAllQuestions();
	public List<Question> listFlaggedQuestions();
	public int addQuestion(Question question);
	public boolean updateQuestion(Question question);
	public boolean deleteQuestion(int id);
	public List<Question> listQuestionsByForumID(int id);
	public Question findQuestionById(int id);
	public List<Question> listQuestionsByTitle(String searchQuery);
	public void flagQuestion(int questionID, String flagged);
	public void likeQuestion(int like, int questionID);
	public void favoriteQuestion(int userID, int questionID);
}
