package com.tiarajarvis.codelet.service;

import java.util.List;

import org.eclipse.jdt.internal.compiler.ast.ReturnStatement;

import com.tiarajarvis.codelet.model.User;

public interface UserService {
	public int addNewUser(User user);
	public boolean updateUser(int id, User user);
	public boolean editProfile(int id, User user);
	public boolean createUserProfile(int id,  User user);
	public boolean deleteUser(int id);
	public User findUserById(int id);
	public User findUserByEmail(String email);
	public List<User> listAdmins();
	public User validateUserLogin(String email, String password);
}
