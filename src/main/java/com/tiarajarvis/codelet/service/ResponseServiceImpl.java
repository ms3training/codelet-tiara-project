package com.tiarajarvis.codelet.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.tiarajarvis.codelet.dao.ResponseDao;
import com.tiarajarvis.codelet.model.Response;

public class ResponseServiceImpl implements ResponseService {

	ResponseDao responseDao;
	
	@Autowired
	public void setResponseDao(ResponseDao responseDao) {
		this.responseDao = responseDao;
	}
	
	public int addResponse(Response response) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
		Date date = new Date();
		response.setPostDate(dateFormat.format(date));
		response.setLikes(0);
		return responseDao.addResponse(response);
	}

	public List<Response> listResponsesByQuestionId(int id) {
		return responseDao.listResponsesByQuestionId(id);
	}

}
