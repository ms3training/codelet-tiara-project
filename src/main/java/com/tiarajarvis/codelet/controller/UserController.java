package com.tiarajarvis.codelet.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tiarajarvis.codelet.model.Challenge;
import com.tiarajarvis.codelet.model.FavoritedQuestion;
import com.tiarajarvis.codelet.model.Forum;
import com.tiarajarvis.codelet.model.Question;
import com.tiarajarvis.codelet.model.Resource;
import com.tiarajarvis.codelet.model.Response;
import com.tiarajarvis.codelet.model.User;
import com.tiarajarvis.codelet.service.ChallengeService;
import com.tiarajarvis.codelet.service.ForumService;
import com.tiarajarvis.codelet.service.QuestionService;
import com.tiarajarvis.codelet.service.ResourceService;
import com.tiarajarvis.codelet.service.ResponseService;
import com.tiarajarvis.codelet.service.UserService;

@Controller
@RequestMapping("/user-home")
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	ForumService forumService;
	
	@Autowired
	QuestionService questionService;
	
	@Autowired
	ResponseService responseService;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ChallengeService challengeService;
	
	protected ModelAndView getUserSession(HttpSession session) {
		ModelAndView model = new ModelAndView();
		User user = (User)session.getAttribute("user");
		model.addObject("userID", user.getUserID());
		model.addObject("firstName", user.getFirstName());
		model.addObject("lastName", user.getLastName());
		model.addObject("bio", user.getBio());
		model.addObject("codeLevel", user.getCodeLevel());
		model.addObject("picture", user.getPicture());
		model.addObject("email", user.getEmail());
		model.addObject("password", user.getPassword());
		return model;
	}
	
	protected User getUserObj(HttpSession session) {
		User user = (User)session.getAttribute("user");
		return user;
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView userHome(@ModelAttribute("errorMessage") String msg, HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<Forum> forumList = forumService.listAllForums();
		List<Forum> resultList = (List<Forum>)session.getAttribute("resultList");
		if(resultList != null){
			model.addObject("forumList", resultList);
		}
		else if(forumList != null){
			model.addObject("forumList", forumList);
		}
		else{
			model.addObject("errorMessage","No Forums");
		}
		return model;
	}
	
	@RequestMapping(value = "/searchForum", method = RequestMethod.POST)
	public ModelAndView searchForumsByTitle(@ModelAttribute("forum") Forum forum, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Forum> resultList = forumService.searchForums(forum.getTitle());		
		ModelAndView model = null;
		if (resultList != null) {
			model = new ModelAndView("redirect:/user-home");
			session.setAttribute("resultList", resultList);
		} else {
			model = new ModelAndView("redirect:/user-home");
			session.setAttribute("resultList", null);
			model.addObject("errorMessage", "No forums match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/filterForumCategory", method = RequestMethod.POST)
	public ModelAndView searchForumsByCategory(@ModelAttribute("forum") Forum forum, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home");
		session.setAttribute("resultList", forumService.listForumsByCategory(forum.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/question/flagQuestion", method = RequestMethod.POST)
	public ModelAndView flagQuestion(@ModelAttribute("question") Question question, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("redirect:/user-home/question/" + question.getQuestionID());
		questionService.flagQuestion(question.getQuestionID(),"Yes");
		return model;
	}
	
	@RequestMapping(value = "/question/likeQuestion", method = RequestMethod.POST)
	public ModelAndView likeQuestion(@ModelAttribute("question") Question question, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("redirect:/user-home/question/" + question.getQuestionID());
		questionService.likeQuestion(question.getLikes() + 1, question.getQuestionID());
		return model;
	}
	
	@RequestMapping(value = "/question/favoriteQuestion", method = RequestMethod.POST)
	public ModelAndView favoriteQuestion(@ModelAttribute("question") Question question, @ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("redirect:/user-home/question/" + question.getQuestionID());
		questionService.favoriteQuestion(user.getUserID(), question.getQuestionID());
		return model;
	}
	
	@RequestMapping(value = "/myQuestions", method = RequestMethod.GET)
	public ModelAndView myQuestions(HttpSession session) {
		ModelAndView model = getUserSession(session);
		User user = getUserObj(session);
		model.setViewName("user-home/myQuestions");
		List<Question> questionList = questionService.listQuestionsByUserID(user.getUserID());
		List<FavoritedQuestion> favoriteList = questionService.listFavoriteQuestionsByID(user.getUserID());
		if(questionList != null){
			model.addObject("questionList", questionList);
		}
		else {
			model.addObject("errorMessage", "You have no questions!");
		}
		
		if(favoriteList != null){
			model.addObject("favoriteList", favoriteList);
		}
		else {
			model.addObject("errorMessageFav", "You have no favorites!");
		}
		return model;
	}
	
	@RequestMapping(value = "/user-settings", method = RequestMethod.GET)
	public ModelAndView settings(HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("user-home/user-settings");
		model.addObject("msg", "none");
		return model;
	}
	
	@RequestMapping(value = "/user-settings/editProfile", method = RequestMethod.POST)
	public ModelAndView editProfile(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
     	userService.editProfile(user.getUserID(), user);
     	model.addObject("msg", "block");
     	session.setAttribute("user", user);
     	model.setViewName("user-home/user-settings");
		return model;
	}
	
	@RequestMapping(value = "/forum/addForum", method = RequestMethod.POST)
	public ModelAndView addForum(@ModelAttribute("forum") Forum forum, HttpSession session) {
		ModelAndView model = getUserSession(session);	
		forumService.addForum(forum);
		model.setViewName("redirect:/user-home/forum/");	
		return model;
	}
	
	@RequestMapping(value = "/user-settings/deleteUser", method = RequestMethod.POST)
	public ModelAndView deleteUser(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		userService.deleteUser(user.getUserID());
		session.setAttribute("user", null);
		model.addObject("msg", "none");
		model.setViewName("redirect:/");
		return model;
	}
	
	@RequestMapping(value = "/myQuestions", method = RequestMethod.POST)
	public ModelAndView manageMyQuestions(@ModelAttribute("questionForm") Question question, HttpSession session) {
		ModelAndView model = getUserSession(session);
		if(question.getTitle() == null){
			questionService.deleteQuestion(question.getQuestionID());
		}
		else{
			questionService.updateQuestion(question);
		}
		model.setViewName("redirect:/user-home/myQuestions");
		return model;
	}
	
	@RequestMapping(value = "/forum/{forumID}", method = RequestMethod.GET)
	public ModelAndView viewForum(@PathVariable("forumID") int id, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("user-home/forum");		
		Forum forum = forumService.findForumById(id);	
		model.addObject("forumID", forum.getForumID());
		model.addObject("forumTitle", forum.getTitle());
		model.addObject("forumDescription", forum.getDescription());			
		List<Question> questionList = questionService.listQuestionsByForumID(id);
		if(questionList != null){
			model.addObject("questionList", questionList);
		}
		else {
			model.addObject("errorMessage", "No questions");
		}
		return model;
	}
	
	@RequestMapping(value = "/forum/{forumID}", method = RequestMethod.POST)
	public ModelAndView addForumQuestion(@ModelAttribute("questionForm") Question question, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("redirect:/user-home/forum/" + question.getForumID());		
		questionService.addQuestion(question);
		return model;
	}
	
	@RequestMapping(value = "/question/{questionID}", method = RequestMethod.GET)
	public ModelAndView viewQuestion(@PathVariable("questionID") int id, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("user-home/question");
		Question question = questionService.findQuestionById(id);	
		User user = userService.findUserById(question.getUserID());
		model.addObject("questionID", question.getQuestionID());
		model.addObject("questionTitle", question.getTitle());
		model.addObject("questionContent", question.getContent());
		model.addObject("questionLikes", question.getLikes());
		model.addObject("firstName", user.getFirstName());
		model.addObject("lastName", user.getLastName());
		List<Response> responseList = responseService.listResponsesByQuestionId(id);
		if(responseList != null){
			model.addObject("responseList", responseList);
		}
		else {
			model.addObject("errorMessage", "No responses");
		}
		return model;
	}
	
	@RequestMapping(value = "/question/{questionID}", method = RequestMethod.POST)
	public ModelAndView addResponse(@ModelAttribute("responseForm") Response response, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("redirect:/user-home/question/" + response.getQuestionID());
		responseService.addResponse(response);
		return model;
	}
	
	@RequestMapping(value = "/forum/{forumID}/search", method = RequestMethod.POST)
	public ModelAndView searchQuestionsByTitle(@ModelAttribute("questions") Question question, HttpSession session) {
		List<Question> questionList = questionService.listQuestionsByTitle(question.getTitle());	
		ModelAndView model = null;
		if (questionList != null) {
			model = new ModelAndView("redirect:/user-home/forum/{forumID}");
			session.setAttribute("questionList", questionList);
		} else {
			model = new ModelAndView("redirect:/user-home/challenges/");
			session.setAttribute("challengeList", null);
			model.addObject("errorMessage", "No challenges match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/challenges", method = RequestMethod.GET)
	public ModelAndView challenges(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<Challenge> allChallengeList = challengeService.listAllChallenges();
		List<Challenge> challengeList = (List<Challenge>)session.getAttribute("challengeList");
		if(challengeList != null){
			model.addObject("challengeList", challengeList);
		}
		else if(allChallengeList != null){
			model.addObject("challengeList", allChallengeList);
		}
		else{
			model.addObject("errorMessage", "No challenges");
		}
		return model;
	}
	
	@RequestMapping(value = "/challenges", method = RequestMethod.POST)
	public ModelAndView addChallenge(@ModelAttribute("challenge") Challenge challenge, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home/challenges/");
		session.setAttribute("challengeList", null);
		challengeService.addChallenge(challenge);		
		return model;
	}
	
	@RequestMapping(value = "/challenges/searchChallenges", method = RequestMethod.POST)
	public ModelAndView searchChallengesByTitle(@ModelAttribute("challenge") Challenge challenge, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Challenge> challengeList = challengeService.listChallengesByTitle(challenge.getTitle());	
		ModelAndView model = null;
		if (challengeList != null) {
			model = new ModelAndView("redirect:/user-home/challenges/");
			session.setAttribute("challengeList", challengeList);
		} else {
			model = new ModelAndView("redirect:/user-home/challenges/");
			session.setAttribute("challengeList", null);
			model.addObject("errorMessage", "No challenges match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/challenges/filterChallengeCategory", method = RequestMethod.POST)
	public ModelAndView searchChallengesByCategory(@ModelAttribute("challenge") Challenge challenge, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home/challenges/");
		session.setAttribute("challengeList", challengeService.listChallengesByCategory(challenge.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/resources", method = RequestMethod.GET)
	public ModelAndView resources(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<Resource> allResourcesList = resourceService.listAllResources();
		List<Resource> resourceList = (List<Resource>)session.getAttribute("resourceList");
		if(resourceList != null){
			model.addObject("resourceList", resourceList);
		}
		else if(allResourcesList != null){
			model.addObject("resourceList", allResourcesList);
		}
		else{
			model.addObject("errorMessage","No Resources");
		}
		return model;
	}
	
	@RequestMapping(value = "/resources", method = RequestMethod.POST)
	public ModelAndView addResources(@ModelAttribute("resources") Resource resources, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home/resources/");
		session.setAttribute("resourceList", null);
		resourceService.addResource(resources);		
		return model;
	}
	
	@RequestMapping(value = "/resources/searchResources", method = RequestMethod.POST)
	public ModelAndView searchResourcesByTitle(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Resource> resourceList = resourceService.listResourcesByTitle(resource.getTitle());	
		ModelAndView model = null;
		if (resourceList != null) {
			model = new ModelAndView("redirect:/user-home/resources/");
			session.setAttribute("resourceList", resourceList);
		} else {
			model = new ModelAndView("redirect:/user-home/resources/");
			session.setAttribute("resourceList", null);
			model.addObject("errorMessage", "No resources match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/resources/filterResourceCategory", method = RequestMethod.POST)
	public ModelAndView searchResourcesByCategory(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home/resources/");
		session.setAttribute("resourceList", resourceService.listResourcesByCategory(resource.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/user-home/clearForumFilters", method = RequestMethod.POST)
	public void clearForumFilters(HttpSession session) {
		session.setAttribute("resultList", null);
	}
	
	@RequestMapping(value = "/user-home/clearResourceFilters", method = RequestMethod.POST)
	public void clearResourceFilters(HttpSession session) {
		session.setAttribute("resourceList", null);
	}
	
	@RequestMapping(value = "/user-home/clearChallengeFilters", method = RequestMethod.POST)
	public void clearChallengeFilters(HttpSession session) {
		session.setAttribute("challengeList", null);
	}
}
