package com.tiarajarvis.codelet.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tiarajarvis.codelet.model.Challenge;
import com.tiarajarvis.codelet.model.Forum;
import com.tiarajarvis.codelet.model.Question;
import com.tiarajarvis.codelet.model.Resource;
import com.tiarajarvis.codelet.model.User;
import com.tiarajarvis.codelet.service.ChallengeService;
import com.tiarajarvis.codelet.service.ForumService;
import com.tiarajarvis.codelet.service.QuestionService;
import com.tiarajarvis.codelet.service.ResourceService;
import com.tiarajarvis.codelet.service.ResponseService;
import com.tiarajarvis.codelet.service.UserService;

@Controller
@RequestMapping("/admin-home")
public class AdminController {

	@Autowired
	UserService userService;
	
	@Autowired
	ForumService forumService;
	
	@Autowired
	QuestionService questionService;
	
	@Autowired
	ResponseService responseService;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ChallengeService challengeService;

	protected ModelAndView getUserSession(HttpSession session) {
		ModelAndView model = new ModelAndView();
		User user = (User)session.getAttribute("user");
		model.addObject("userID", user.getUserID());
		model.addObject("firstName", user.getFirstName());
		model.addObject("lastName", user.getLastName());
		model.addObject("bio", user.getBio());
		model.addObject("codeLevel", user.getCodeLevel());
		model.addObject("picture", user.getPicture());
		model.addObject("email", user.getEmail());
		model.addObject("password", user.getPassword());
		return model;
	}
	
	protected void clearSearch(HttpSession session) {
		session.setAttribute("user", null);
		session.setAttribute("resultList", null);
		session.setAttribute("resourceList", null);
	}
	protected User getUserObj(HttpSession session) {
		User user = (User)session.getAttribute("user");
		return user;
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView adminHome(@ModelAttribute("errorMessage") String msg, HttpSession session) {
		ModelAndView model = getUserSession(session);
		return model;
	}
	
	//FORUMS
	@RequestMapping(value = "/forums", method = RequestMethod.GET)
	public ModelAndView forums(HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<Forum> forumList = forumService.listAllForums();
		List<Forum> resultList = (List<Forum>)session.getAttribute("resultList");
		if(resultList != null){
			model.addObject("forumList", resultList);
		}
		else if(forumList != null){
			model.addObject("forumList", forumList);
		}
		else{
			model.addObject("errorMessage","No Forums");
		}
		return model;
	}
	
	@RequestMapping(value = "/forums", method = RequestMethod.POST)
	public ModelAndView manageForums(@ModelAttribute("forum") Forum forum, HttpSession session) {
		ModelAndView model = getUserSession(session);
		if(forum.getTitle() == null){
			forumService.deleteForum(forum.getForumID());
		}
		else{
			forumService.updateForum(forum);
		}
		model.setViewName("redirect:/admin-home/forums");
		return model;
	}
	
	@RequestMapping(value = "/forums/searchForum", method = RequestMethod.POST)
	public ModelAndView searchForumsByTitle(@ModelAttribute("forum") Forum forum, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Forum> resultList = forumService.searchForums(forum.getTitle());		
		ModelAndView model = null;
		if (resultList != null) {
			model = new ModelAndView("redirect:/admin-home/forums/");
			session.setAttribute("resultList", resultList);
		} else {
			model = new ModelAndView("redirect:/admin-home/forums/");
			session.setAttribute("resultList", null);
			model.addObject("errorMessage", "No forums match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/forums/filterForumCategory", method = RequestMethod.POST)
	public ModelAndView searchForumsByCategory(@ModelAttribute("forum") Forum forum, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/forums/");
		session.setAttribute("resultList", forumService.listForumsByCategory(forum.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/forums/addForum", method = RequestMethod.POST)
	public ModelAndView addForum(@ModelAttribute("forum") Forum forum, HttpSession session) {
		ModelAndView model = getUserSession(session);	
		forumService.addForum(forum);
		model.setViewName("redirect:/admin-home/forums/");	
		return model;
	}
	
	@RequestMapping(value = "/questions", method = RequestMethod.GET)
	public ModelAndView questions(HttpSession session) {
		//clearSearch(session);
		ModelAndView model = getUserSession(session);
		model.setViewName("admin-home/questions");
		List<Question> questionList = questionService.listAllQuestions();
		List<Question> flaggedList = questionService.listFlaggedQuestions();
		model.addObject("flaggedList", flaggedList);
		if(questionList != null){
			model.addObject("questionList", questionList);
		}
		else {
			model.addObject("errorMessage", "No questions!");
			model.addObject("display", "block");
		}
		return model;
	}
	
	@RequestMapping(value = "/questions", method = RequestMethod.POST)
	public ModelAndView manageMyQuestions(@ModelAttribute("questionForm") Question question, HttpSession session) {
		//clearSearch(session);
		ModelAndView model = getUserSession(session);
		if(question.getTitle() == null){
			questionService.deleteQuestion(question.getQuestionID());
		}
		else{
			questionService.updateQuestion(question);
		}
		model.setViewName("redirect:/admin-home/questions");
		return model;
	}
	
	@RequestMapping(value = "/questions/searchQuestions", method = RequestMethod.POST)
	public ModelAndView searchQuestionsByTitle(@ModelAttribute("question") Question question, HttpSession session) {
		List<Question> questionList = questionService.listQuestionsByTitle(question.getTitle());
		ModelAndView model = null;
		if (questionList != null) {
			model = new ModelAndView("redirect:/admin-home/questions/");
			session.setAttribute("questionList", questionList);
		} else {
			model = new ModelAndView("redirect:/admin-home/questions/");
			session.setAttribute("resultList", null);
			model.addObject("errorMessage", "No questions match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/resources", method = RequestMethod.GET)
	public ModelAndView resources(@ModelAttribute("user") User user, HttpSession session) {
		//clearSearch(session);
		ModelAndView model = getUserSession(session);
		List<Resource> allResourcesList = resourceService.listAllResources();
		List<Resource> resourceList = (List<Resource>)session.getAttribute("resourceList");
		if(resourceList != null){
			model.addObject("resourceList", resourceList);
		}
		else if(allResourcesList != null){
			model.addObject("resourceList", allResourcesList);
		}
		else{
			model.addObject("errorMessage","No Resources");
		}
		return model;
	}
	
	@RequestMapping(value = "/resources", method = RequestMethod.POST)
	public ModelAndView addResources(@ModelAttribute("resources") Resource resources, HttpSession session) {
		//clearSearch(session);
		ModelAndView model = new ModelAndView("redirect:/admin-home/resources/");
		resourceService.addResource(resources);		
		return model;
	}
	
	@RequestMapping(value = "/resources/searchResources", method = RequestMethod.POST)
	public ModelAndView searchResourcesByTitle(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Resource> resourceList = resourceService.listResourcesByTitle(resource.getTitle());	
		ModelAndView model = null;
		if (resourceList != null) {
			model = new ModelAndView("redirect:/admin-home/resources/");
			session.setAttribute("resourceList", resourceList);
		} else {
			model = new ModelAndView("redirect:/admin-home/resources/");
			session.setAttribute("resourceList", null);
			model.addObject("errorMessage", "No resources match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/resources/filterResourceCategory", method = RequestMethod.POST)
	public ModelAndView searchResourcesByCategory(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/resources/");
		session.setAttribute("resourceList", resourceService.listResourcesByCategory(resource.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/resources/addResource", method = RequestMethod.POST)
	public ModelAndView addResource(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/resources/");
		resourceService.addResource(resource);		
		return model;
	}
	
	@RequestMapping(value = "/resources/editResource", method = RequestMethod.POST)
	public ModelAndView editResource(@ModelAttribute("resource") Resource resource, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/resources/");
		resourceService.updateResource(resource);
		return model;
	}
	
	@RequestMapping(value = "/resources/deleteResource", method = RequestMethod.POST)
	public ModelAndView deleteResource(@ModelAttribute("resource") Resource resource, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/resources/");
		resourceService.deleteResource(resource.getResourceID());
		return model;
	}
	
	@RequestMapping(value = "/challenges", method = RequestMethod.GET)
	public ModelAndView challenges(@ModelAttribute("challengeForm") Challenge challenge, HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<Challenge> allChallengeList = challengeService.listAllChallenges();
		List<Challenge> challengeList = (List<Challenge>)session.getAttribute("challengeList");
		if(challengeList != null){
			model.addObject("challengeList", challengeList);
		}
		else if(allChallengeList != null){
			model.addObject("challengeList", allChallengeList);
		}
		else{
			model.addObject("errorMessage", "No challenges");
		}
		return model;
	}
	
	@RequestMapping(value = "/challenges/searchChallenges", method = RequestMethod.POST)
	public ModelAndView searchChallengesByTitle(@ModelAttribute("challenge") Challenge challenge, RedirectAttributes redirectAttributes, HttpSession session) {
		List<Challenge> challengeList = challengeService.listChallengesByTitle(challenge.getTitle());	
		ModelAndView model = null;
		if (challengeList != null) {
			model = new ModelAndView("redirect:/admin-home/challenges/");
			session.setAttribute("challengeList", challengeList);
		} else {
			model = new ModelAndView("redirect:/admin-home/challenges/");
			session.setAttribute("challengeList", null);
			model.addObject("errorMessage", "No challenges match your search :(");
		}
		return model;
	}
	
	@RequestMapping(value = "/challenges/addChallenge", method = RequestMethod.POST)
	public ModelAndView addChallenge(@ModelAttribute("challenge") Challenge challenge, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/challenges/");
		challengeService.addChallenge(challenge);
		return model;
	}
	
	@RequestMapping(value = "/challenges/filterChallengeCategory", method = RequestMethod.POST)
	public ModelAndView searchChallengesByCategory(@ModelAttribute("challenge") Challenge challenge, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/challenges/");
		session.setAttribute("challengeList", challengeService.listChallengesByCategory(challenge.getCategory()));
		return model;
	}
	
	@RequestMapping(value = "/challenges/editChallenge", method = RequestMethod.POST)
	public ModelAndView editChallenge(@ModelAttribute("challenge") Challenge challenge, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/challenges/");
		challengeService.updateChallenge(challenge);
		return model;
	}
	
	@RequestMapping(value = "/challenges/deleteChallenge", method = RequestMethod.POST)
	public ModelAndView deleteChallenge(@ModelAttribute("challenge") Challenge challenge, RedirectAttributes redirectAttributes, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/admin-home/challenges/");
		challengeService.deleteChallenge(challenge.getChallengeID());
		return model;
	}
	
	@RequestMapping(value = "/administrators", method = RequestMethod.GET)
	public ModelAndView administrators(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		List<User> userList = userService.listAdmins();
		model.addObject("userList", userList);
		model.setViewName("admin-home/administrators");
		return model;
	}
	
	@RequestMapping(value = "/administrators", method = RequestMethod.POST)
	public ModelAndView manageAdministrators(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		if(user.getUserID() != 0){	
			userService.deleteUser(user.getUserID());
		}
		else{
			user.setUserRoleID("admin");
			userService.addNewUser(user);
			
		}
		model.setViewName("admin-home/administrators");
		return model;
	}
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ModelAndView settings(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		model.setViewName("admin-home/settings");
		model.addObject("msg", "none");
		return model;
	}
	
	@RequestMapping(value = "/settings", method = RequestMethod.POST)
	public ModelAndView editProfile(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = getUserSession(session);
		session.setAttribute("user", user);
		userService.editProfile(user.getUserID(), user);
 		model.addObject("msg", "block");
		return model;
	}
}
