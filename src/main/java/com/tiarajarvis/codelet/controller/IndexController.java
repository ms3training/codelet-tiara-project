package com.tiarajarvis.codelet.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tiarajarvis.codelet.model.User;
import com.tiarajarvis.codelet.service.UserService;

@Controller
public class IndexController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView welcome(HttpSession session) {
		session.setAttribute("user", null);
		session.setAttribute("resultList", null);
		session.setAttribute("resourceList", null);
		return new ModelAndView("index");
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView showRegisterForm() {
		ModelAndView model = new ModelAndView("register");
		model.addObject("user", new User());
		return model;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView addUser(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes) {
		ModelAndView model = new ModelAndView("redirect:/profile");
		redirectAttributes.addFlashAttribute("userID", userService.addNewUser(user));	
		return model;
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView showProfileForm(@ModelAttribute("userID") int userID) {
		ModelAndView model = new ModelAndView("profile");
		model.addObject("userID", userID);
		return model;
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public ModelAndView createProfile(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView model = new ModelAndView("redirect:/user-home");
		userService.createUserProfile(user.getUserID(), user);
		User userProfile = userService.findUserById(user.getUserID());
		session.setAttribute("user", userProfile);
		return model;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLoginForm() {
		ModelAndView model = new ModelAndView("login");
		model.addObject("user", new User());
		model.addObject("display", "none");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView loginProcess(@ModelAttribute("user") User user, HttpSession session) {
		User userProfile = userService.validateUserLogin(user.getEmail(), user.getPassword()); 
		ModelAndView model = null;
		if (userProfile != null) {
			if(userProfile.getUserRole().equals("regular")){
				model = new ModelAndView("redirect:/user-home");				
			}
			else{
				model = new ModelAndView("redirect:/admin-home");
			}	
			session.setAttribute("user", userProfile);
		} else {
			model = new ModelAndView("login");
			model.addObject("display", "block");
			model.addObject("errorMessage","Incorrect email and password. Please try again.");
		}
		return model;
	}
}
