<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en" class="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Codelet</title>
<spring:url value="/resources/css/Main.css" var="mainCss" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<spring:url value="/resources/js/Main.js" var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${jqueryJs}"></script>
<script src="${mainJs}"></script>
</head>
<body class="html codelet-bg">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><i class="fa fa-code fa-2x"
					aria-hidden="true"></i> <span style="font-size: 1.7em;">Codelet</span></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right nav-pills">
					<li><a class="btn btn-secondary btn-home" href="login">Login</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="spacer"></div>
	<div>
		<div class="container">
			<div class="login col-sm-6 col-sm-offset-3">
				<div>

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#login"
							aria-controls="login" role="tab" data-toggle="tab">Login</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="login">
							<br /> <br />
							<h3 class="text-center">Welcome Back!</h3>
							<br /> <br />
							<form>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i
													class="fa fa-envelope" aria-hidden="true"></i></span> <input
													name="email" type="text" class="form-control"
													placeholder="email" required>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-lock"
													aria-hidden="true"></i></span> <input name="password"
													type="password" class="form-control" placeholder="password"
													required>
											</div>
										</div>
									</div>
								</div>
								<input name="submit" type="submit" value="Submit"
									class="form-control btn btn-secondary" />
							</form>
							<br />
							<h5 class="text-center" style="color: #fff; font-size: 16px;">
								Don't have an account? Sign up <b><a href="register"
									style="text-decoration: underline" class="mainblue">here</a>&nbsp;<i
									class="fa fa-pencil" aria-hidden="true"></i></b>
							</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>




