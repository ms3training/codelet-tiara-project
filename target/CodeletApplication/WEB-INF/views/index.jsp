<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Codelet</title>
    <spring:url value="/resources/css/Main.css" var="mainCss" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<spring:url value="/resources/js/Main.js" var="mainJs" />

	<link href="${mainCss}" rel="stylesheet" />
    <script src="${jqueryJs}"></script>
    <script src="${mainJs}"></script>
</head>
<body class="html codelet-bg">
    <form id="form1">
            <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><i class="fa fa-code fa-2x" aria-hidden="true"></i> <span style="font-size:1.7em;">Codelet</span></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a class="btn btn-secondary btn-home" href="login">Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
            <div class="spacer"></div>
            <div>
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="jumbotron">
                    <div class="carousel-inner text-center">
                        <div class="item active">
                        <br/><br/><br/>
                        <i class="fa fa-code fa-4x" aria-hidden="true"></i>
                          <h1 class="mainblue">Codelet</h1>
                          <h3>develop, learn &amp; share code</h3>
                          <br/><br/>
                          <a class="btn btn-secondary btn-lg" href="login">Get Started</a>
                      </div>

                      <div class="item">
                          <br/><br/><br/>
                          <i class="fa fa-question-circle fa-4x" aria-hidden="true"></i>
                          <h1 class="mainblue">Need Help?</h1>
                          <h3>ask other developers for help on your problems</h3>
                          <br/><br/>
                          <a class="btn btn-secondary btn-lg" href="login">Ask a question</a>
                      </div>

                      <div class="item">
                          <br/><br/><br/>
                          <i class="fa fa-trophy fa-4x" aria-hidden="true"></i>
                          <h1 class="mainblue">Challenge Yourself</h1>
                          <h3>test your knowledge against our coding challenges</h3>
                          <br/><br/>
                          <a class="btn btn-secondary btn-lg" href="login">Practice</a>
                      </div>
                  </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</form>
</body>
</html>
