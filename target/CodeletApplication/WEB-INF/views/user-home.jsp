<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Codelet</title>
  <link rel="stylesheet" href="assets/css/Main.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="html bg">
  <form id="form1">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html"><i class="fa fa-code fa-2x" aria-hidden="true"></i> <span style="font-size:1.7em;">Codelet</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">3</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 3 new responses</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- start message -->
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Matt Murgia
                              <small><i class="fa fa-clock-o"></i> 5 mins</small>
                            </h4>
                            <p>Why not use API PRO?</p>
                          </a>
                        </li>
                        <!-- end message -->
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Aaron Weikle
                              <small><i class="fa fa-clock-o"></i> 2 hours</small>
                            </h4>
                            <p>Anypoint is the way to go!</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Cyril Thorton
                              <small><i class="fa fa-clock-o"></i> Today</small>
                            </h4>
                            <p>I agree with Aaron</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">See All Responses</a></li>
                  </ul>
                </li>

                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/user.png" class="user-image" alt="User Image" width="160" height="160" style="background-color: #fff;">
                    <span class="hidden-xs">Tiara Jarvis</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="assets/images/user.png" class="user-image" alt="User Image" width="160" height="160" style="background-color: #fff;">
                      <p>Tiara Jarvis<br/>Jr. Software Engineer</p> 
                      <small>Code Level: Intermediate</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-secondary btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="index.html" class="btn btn-secondary btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    <div id="title-header">
      <div>
        <h1 style="color: #000;"><i class="fa fa-rss" aria-hidden="true"></i> Forums</h1>
      </div>
    </div>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#secondary-nav" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse navbar-center" id="secondary-nav">
          <ul class="nav navbar-nav child">
            <li><a href="home.html"><i class="fa fa-rss" aria-hidden="true"></i> Forums</a></li>
            <li><a href="myQuestions.html"><i class="fa fa-question" aria-hidden="true"></i> My Questions</a></li>
            <li><a href="resources.html"><i class="fa fa-files-o" aria-hidden="true"></i> Docs &amp; Videos</a></li>
            <li><a href="challenges.html"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </nav>
  <div class="gray-bg">
    <div class="container">
      <div class="content-bg">
        <br/>   
        <div class="form-group">
          <div class="row">
            <div class="col-sm-8">
              <input type="text" class="form-control" placeholder="e.g. Spring Framework">
            </div>
            <div class="col-sm-4">
              <a href="#" class="btn btn-secondary form-control">Search <i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
        <br/>
        <div id="categories">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-2">     
                <div class="forum-category green-category">
                  <br/>
                  <i class="fa fa-gamepad fa-3x" aria-hidden="true"></i>
                  <p><a href="#">Game Development</a></p>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="forum-category lblue-category">
                  <br/>
                  <i class="fa fa-database fa-3x" aria-hidden="true"></i>
                  <p><a href="#">Database</a></p>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="forum-category pink-category">
                  <br/>
                  <i class="fa fa-mobile fa-3x" aria-hidden="true"></i>
                  <p><a href="#">Mobile Development</a></p>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="forum-category yellow-category">
                  <br/>
                  <i class="fa fa-file-code-o fa-3x" aria-hidden="true"></i>
                  <p><a href="#">Languages</a></p>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="forum-category teal-category">
                  <br/>
                  <i class="fa fa-list-alt fa-3x" aria-hidden="true"></i>
                  <p><a href="#">QA Testing</a></p>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="forum-category purple-category">
                  <br/>
                  <i class="fa fa-wrench fa-3x" aria-hidden="true"></i>
                  <p><a href="#">Development Tools</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br/>
        <div id="searchResults">
          <div class="card">
            <h4><a href="forum.html" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 213</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 0</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 2,565</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 489</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 7</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 584</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <br/>
          <div class="card">
            <h4><a href="#" class="pull-left">Forum Title</a></h4>
            <span class="label label-questions pull-right"><i class="fa fa-comments" aria-hidden="true"></i> 1,003</span>
            <br/>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
        </div>
        <br/>
        <div id="pagination" class="text-center">
          <nav>
            <ul class="pagination">
              <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active">
                <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
              </li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</form>
</body>
</html>




