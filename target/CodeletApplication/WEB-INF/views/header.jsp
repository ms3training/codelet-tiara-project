<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Codelet</title>
  <link rel="stylesheet" href="assets/css/Main.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="html bg">
  <form id="form1">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html"><i class="fa fa-code fa-2x" aria-hidden="true"></i> <span style="font-size:1.7em;">Codelet</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">3</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 3 new responses</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- start message -->
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Matt Murgia
                              <small><i class="fa fa-clock-o"></i> 5 mins</small>
                            </h4>
                            <p>Why not use API PRO?</p>
                          </a>
                        </li>
                        <!-- end message -->
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Aaron Weikle
                              <small><i class="fa fa-clock-o"></i> 2 hours</small>
                            </h4>
                            <p>Anypoint is the way to go!</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="assets/images/user.png" class="img-circle" alt="User Image" width="128" height="128" style="background-color: #fff;">
                            </div>
                            <h4>
                              Cyril Thorton
                              <small><i class="fa fa-clock-o"></i> Today</small>
                            </h4>
                            <p>I agree with Aaron</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">See All Responses</a></li>
                  </ul>
                </li>

                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/user.png" class="user-image" alt="User Image" width="160" height="160" style="background-color: #fff;">
                    <span class="hidden-xs">Tiara Jarvis</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="assets/images/user.png" class="user-image" alt="User Image" width="160" height="160" style="background-color: #fff;">
                      <p>Tiara Jarvis<br/>Jr. Software Engineer</p> 
                      <small>Code Level: Intermediate</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-secondary btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="index.html" class="btn btn-secondary btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    <div id="title-header">
      <div>
        <h1 style="color: #000;"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</h1>
      </div>
    </div>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#secondary-nav" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse navbar-center" id="secondary-nav">
          <ul class="nav navbar-nav child">
            <li><a href="home.html"><i class="fa fa-rss" aria-hidden="true"></i> Forums</a></li>
            <li><a href="myQuestions.html"><i class="fa fa-question" aria-hidden="true"></i> My Questions</a></li>
            <li><a href="resources.html"><i class="fa fa-files-o" aria-hidden="true"></i> Docs &amp; Videos</a></li>
            <li><a href="challenges.html"><i class="fa fa-trophy" aria-hidden="true"></i> Challenges</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </nav>